#!/bin/bash
#
#SBATCH --job-name=varAlg
#SBATCH --output=slurm_output/variational_algorithm/stdout_JOB_%A.txt
#SBATCH --error=slurm_output/variational_algorithm/stderr_JOB_%A.txt
#
#SBATCH --ntasks=1
#SBATCH --time=07-00:00:00 # DD-HH:MM:SS
#SBATCH --mem-per-cpu=250 # MB, TODO: use mprof to check required memory and time

# argument/s: task_id (handle everything else inside python)
srun python3 -u ./scripts/run_variational_algorithm.py
