import numpy as np
from scipy.linalg import sqrtm


def QFImixed_finite(rho1, rho2):
    # TODO: drop negative evals like QuTiP
    # Uses definition of QFI using Bures fidelity.

    # rho = 0.5 * (rho1 + rho2)
    # Nt = len(np.diag(rho)) - 1

    Fid = (np.trace(sqrtm(sqrtm(rho2) @ rho1 @ sqrtm(rho2)))) ** 2
    FQ = 4 * (1 - Fid)

    return FQ
