#!/usr/bin/env python3
import sys, os

# add directory to import path
sys.path.insert(1, os.path.join(sys.path[0], "../python_source"))

from pathlib import Path
import math
import numpy as np

from operators import *
from states import *
from encoding_channels import *
from fisher_information import *
from loss_channels import *

# -----------------------
# ---- user settings ----
# -----------------------

# TODO: write new methods to load .npy and .pkl and use the options from the latter for the former, or just access ket= in .pkl

file = sys.argv[1]
# TODO: use .h5 instead of extracting information from the filename like this
label = Path(file).stem
x = label.split("-")[1].replace(",", "=").split("=")
ks = x[::2]
# converting to float or int appropriately
vs = [float(x) if float(x) - int(float(x)) > 0 else int(float(x)) for x in x[1::2]]
options = dict(zip(ks, vs))
ket = np.load(file)

print(f"Options extacted from label: {options}.")

N = options["N"]
loss_tol = 1e-100
# k = options["k"]
# num_peaks = options["peaks"]
# with_phases = False
# -----------------------
# -----------------------

settings = dict(
    N=N,
    σ=1e-3,
    δσ=1e-5,
    η=1e-1,
    small_signal=True,
    loss_tol=loss_tol,
)
construct_total_channel(settings)

ket_opt_Fock = fock_ket(N, 8)
# print(f"[For optimal Fock state.] From finite difference method, QFI = {QFI_finite_difference(ket_opt_Fock, settings):.3f}.")
# TODO: determine why this does not match the FDM result. Test this on known Fock state QFIs. --> also does not work for Fock state (is this a small signal=True issue? why?)
# print(f"[For optimal Fock state.] From channel derivative, QFI = {QFI_from_Λ_dot(ket_opt_Fock, settings):.3f}.")

print(f"Normalisation, norm = {norm(N, ket)}.")

print(f"From label, QFI = {options['QFI']:.3f}.")
HE_UB = lambda η: 2 / η
Fq = QFI_finite_difference(ket, settings)
print(
    f"From finite difference method with 100 losses, QFI = {Fq:.3f}, HE_UB = {HE_UB(settings['η'])} at η = {settings['η']}, saturation = {Fq/HE_UB(settings['η']):.2%}."
)
# print(f"From channel derivative, QFI = {QFI_from_Λ_dot(ket, settings):.3f}.")
print(f"Number basis CFI = {CFI_Fock_finite_difference(ket, settings):.3f}.")

print("Testing deterministic displacement QFI.")
ρ_after_input_loss = loss_kraus(N, ket, settings["η"], tol=1e-100)
print(f"Rank = {rank(ρ_after_input_loss)}, purity = {purity(ρ_after_input_loss)}.")
Fq = QFI_mixed_state_unitary_encoding(N, ρ_after_input_loss, p_quad(N))
print(
    f"Deterministic displacement in x quadrature, QFI = {Fq:.3f}, HE_UB = {HE_UB(settings['η'])} at η = {settings['η']}, saturation = {Fq/HE_UB(settings['η']):.2%}."
)

print("Testing saturation of HE UB with different losses.")
ηs = [0.11, 0.09, 0.5, 0.2, 0.05, 0.02, 0.01, 1e-3, 1e-4]
for η in ηs:
    modified_settings = {**settings, "η": η}
    construct_total_channel(modified_settings)
    Fq = QFI_finite_difference(ket, modified_settings)
    print(
        f"From finite difference method with 100 losses, QFI = {Fq:.3f}, HE_UB = {HE_UB(modified_settings['η'])} at η = {modified_settings['η']}, saturation = {Fq/HE_UB(modified_settings['η']):.2%}."
    )

print("Testing tolerance to truncation of loss Kraus representation.")
# Small signal approximation is reliable, so don't worry about that.
# 1e-300 is problematic
loss_tols = np.geomspace(1e-100, 1e-300, 8)
for loss_tol in loss_tols:
    updated_settings = {
        **settings,
        # "small_signal":False,
        # "encoding_truncation":3,
        "loss_tol": loss_tol,
    }
    construct_total_channel(updated_settings)
    print(f"tol={loss_tol:.0e}", QFI_finite_difference(ket, updated_settings))

print("Testing tolerance to truncation of Hilbert space in the number basis.")
Ns = [
    N + 20,
]
for N in Ns:
    updated_settings = {
        **settings,
        "N": N,
    }
    construct_total_channel(updated_settings)
    print(f"N={N}", QFI_finite_difference(zero_pad(N, ket), updated_settings))

print("Testing sensitivity to signal parameter σ.")
σs = np.geomspace(1e-2, 1e-7, 6)
Fq00, Fq00_Fock = None, None
for σ in σs:
    updated_settings = {
        **settings,
        "σ": σ,
    }
    construct_total_channel(updated_settings)
    print(f"tol={loss_tol:.0e}", QFI_finite_difference(ket, updated_settings))

    Fq0 = QFI_finite_difference(ket, settings)
    Fq0_Fock = QFI_finite_difference(ket_opt_Fock, settings)
    if Fq00 is None:
        Fq00 = Fq0
    if Fq00_Fock is None:
        Fq00_Fock = Fq0_Fock
    # Shows that a small enough σ causes QFI to drop for all states
    print(
        f"σ={σ:.0e}, (this state) Fq={Fq0:.5f}, ratio={Fq0/Fq00:.3f}; (opt Fock) Fq={Fq0_Fock:.5f}, ratio={Fq0_Fock/Fq00_Fock:.3f}."
    )
