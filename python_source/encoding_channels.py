from operators import *
from states import *
from loss_channels import loss_kraus, loss_kraus_cached

from tqdm.notebook import tqdm


def random_channel(
    ρ, σ, subchannel, use_pdot=False, renormalise=True, αs=None, tqdm_bar=False
):
    """Returns copy of ρ with σ encoded as the Gaussian mixture of component subchannels.

    Needs a and ad defined.

    Args:
        ρ (Nmax-by-Nmax): density matrix
        σ (float): signal parameter
        subchannel (state, float -> state): component channel that depends on α
        use_pdot (bool): whether to use the derivative channel
        renormalise (bool): whether to renormalise the result to the initial norm
    """
    if σ == 0:
        return ρ

    # TODO: increase range if \sigma is large, check norm within range
    # if σ <= 1e-4:
    #     raise ValueError("σ^2 too small numerically and likely to be inaccurate.")
    if αs is None:
        α_max = 0.05
        α_min = -0.05
        if σ > α_max:
            raise ValueError("σ comperable to α range and likely to be inaccurate.")
        α_steps = 500
        α_bin_width = (α_max - α_min) / α_steps
        αs = np.linspace(α_min, α_max, α_steps)
    else:
        α_bin_width = αs[1] - αs[0]

    # TODO: use Gaussian function here instead of hard coding
    # <p> ~ N(0, σ^2) such that α ~ N(0, σ^2 / 2): note that α is α' in the paper
    Pα = 1 / (σ * math.sqrt(np.pi)) * np.exp(-(αs**2) / σ**2)
    # renormalise Riemann integral of pa # TODO: integrate with trapz properly instead of this
    Pα /= sum(Pα * α_bin_width)
    if use_pdot:
        # formula from mathematica derivative of Pα wrt σ
        dotPα = (
            -1
            / (σ**4 * math.sqrt(np.pi))
            * (σ**2 - 2 * αs**2)
            * np.exp(-(αs**2) / σ**2)
        )
        # integral is zero, but let's normalise it with the other norm, TODO: see if this does anything (for 0.01 the integral of p is almost one so the change is minimal)
        # print("Integral should be one: ", sum(Pα * α_bin_width))
        # print("Integral should be zero: ", sum(dotPα * α_bin_width))
        # dotPα /= sum(Pα * α_bin_width)

    ρ_encoded = np.zeros_like(ρ)
    # TODO: implement enumerate(tqdm(αs, desc=f'Integrating over α for σ = {σ:.1g}')) as optional?
    if tqdm_bar:
        αs_enumerated = enumerate(tqdm(αs, desc=f"Integrating over α for σ = {σ:.1g}"))
    else:
        αs_enumerated = enumerate(αs)
    for j, α in αs_enumerated:
        # include the bin width in the Riemann sum
        if not use_pdot:
            weight = Pα[j] * α_bin_width
        else:
            weight = dotPα[j] * α_bin_width
        # print(ρ_encoded, weight, subchannel(ρ, α))
        ρ_encoded += weight * subchannel(ρ, α)
        # TODO: handle the integration using np.trapz correctly instead?

    # renormalise (TODO: check if this handles the constant α_bin_width which should appear in the Riemann sum above)
    # now normalised to have the same as the initial norm to handle the Choi unnormalised state case
    # channel is a CP-TP map, so it preserves the initial trace
    # print(
    #     f"Bin width = {α_bin_width}, Traces: initial = {initial_trace}, final = {final_trace}, ratio = {initial_trace / final_trace}."
    # )
    # print(np.trace(ρ).real, np.trace(ρ_encoded).real, np.trace(ρ).real/ np.trace(ρ_encoded).real)
    if renormalise:
        if use_pdot:
            raise ValueError(
                "Should not renormalise derivative which can be small or zero."
            )
        initial_trace = np.trace(ρ).real
        final_trace = np.trace(ρ_encoded).real
        ρ_encoded *= initial_trace / final_trace
    return ρ_encoded


def encoding_channel(N, ρ, σ, encoding_truncation, along_p=False, **kwargs):
    """Returns copy of ρ with σ encoded.

    Needs a and ad defined.

    Args:
        ρ (Nmax-by-Nmax): density matrix
        σ (float): signal parameter
        encoding_truncation (int): truncation index for displacement operator
    """
    # integrating displaced density matrices against α
    if not along_p:
        # displacing along x although small signal limit below is displacing along p
        subchannel = lambda ρ0, α: displace(N, ρ0, α, encoding_truncation)
    else:
        subchannel = lambda ρ0, α: displace(N, ρ0, 1j * α, encoding_truncation)
    return random_channel(ρ, σ, subchannel, **kwargs)


def encode_Fock_state(N, n, σ):
    """Returns density matrix of Fock state with σ encoded for small σ only.

    Needs Nmax defined.

    Args:
        n (int): input number of particles
        σ (float): signal parameter
    """
    ρ = np.zeros((N, N))

    # hard-coded analytic results from mathematica
    ρ[n, n] = 1 - 0.5 * (1 + 2 * n) * σ**2
    ρ[n - 1, n - 1] = 0.5 * n * σ**2
    ρ[n + 1, n + 1] = 0.5 * (1 + n) * σ**2
    ρ[n, n - 2] = ρ[n - 2, n] = 0.25 * math.sqrt((n - 1) * n) * σ**2
    ρ[n + 1, n - 1] = ρ[n - 1, n + 1] = -0.5 * math.sqrt(n * (n + 1)) * σ**2
    ρ[n + 2, n] = ρ[n, n + 2] = 0.25 * math.sqrt((n + 1) * (n + 2)) * σ**2

    return ρ


def small_signal_encoding_channel(N, state, σ, dot=False, ρ_and_ρ_dot=False):
    """Approximation in small α, i.e. in small σx"""
    # TODO: check when σx rather than just σ is small, use operator trace norm?
    if σ > 1.5e-2:
        raise ValueError("Approximation only valid for small signals.")

    ρ = ρ_from_state(N, state)
    x_sqr = np.linalg.matrix_power(x_quad(N), 2)
    lindblad = x_quad(N) @ ρ @ x_quad(N) - 0.5 * (ρ @ x_sqr + x_sqr @ ρ)
    # TODO: compare this to Tuvia's cos/sin(σ x_quad) two Kraus operator approximation and to the full encoding
    # --> error is too large?
    # TODO: check that there isn't a factor of two missing in the σ definition
    # this is naturally trace preserving since the added term has zero trace
    if ρ_and_ρ_dot:
        return (ρ + σ**2 * lindblad, 2 * σ * lindblad)
    elif not dot:
        return ρ + σ**2 * lindblad
    else:
        return 2 * σ * lindblad


def small_signal_encoding_channel_2D(N, state, σ, dot=False):
    """Approximation in small α, i.e. in small σx"""
    # TODO: check when σx and σp rather than just σ is small, use operator trace norm?
    if σ > 1.5e-2:
        raise ValueError("Approximation only valid for small signals.")

    ρ = ρ_from_state(N, state)
    x_sqr = np.linalg.matrix_power(x_quad(N), 2)
    lindblad_x = x_quad(N) @ ρ @ x_quad(N) - 0.5 * (ρ @ x_sqr + x_sqr @ ρ)
    p_sqr = np.linalg.matrix_power(p_quad(N), 2)
    lindblad_p = p_quad(N) @ ρ @ p_quad(N) - 0.5 * (ρ @ p_sqr + p_sqr @ ρ)
    lindblad = lindblad_x + lindblad_p
    if not dot:
        return ρ + σ**2 * lindblad
    else:
        return 2 * σ * lindblad


def construct_total_channel(
    settings, in_place=True, σ0=None, renormalise=True, **kwargs
):
    """Create the total channel describing the composition of the loss and encoding channels.

    Args:
        settings (_type_): _description_
        in_place (bool, optional): _description_. Defaults to True.
        renormalise (bool, optional): _description_. Defaults to True.
        kwargs: Options passed to encoding_channel
    """

    def total_channel(state, σ):
        """Returns the state after input loss then encoding channel."""
        if "cache" not in settings.keys() or not settings["cached_losses"]:
            ρ_after_input_loss = loss_kraus(
                settings["N"],
                state,
                settings["η"],
                tol=settings["loss_tol"] if "loss_tol" in settings.keys() else 1e-6,
                renormalise=renormalise,
            )
        else:
            ρ_after_input_loss = loss_kraus_cached(
                settings["N"],
                state,
                settings["cache"],
                renormalise=renormalise,
            )
        if not settings["small_signal"]:
            ρ_final = encoding_channel(
                N=settings["N"],
                ρ=ρ_after_input_loss,
                σ=σ,
                encoding_truncation=settings["encoding_truncation"],
                **kwargs,
            )
        elif "use_2D" not in settings.keys() or not settings["use_2D"]:
            ρ_final = small_signal_encoding_channel(
                settings["N"], ρ_after_input_loss, σ
            )
        else:
            ρ_final = small_signal_encoding_channel_2D(
                settings["N"], ρ_after_input_loss, σ
            )
        return ρ_final

    if σ0 is not None:
        fn = lambda state: total_channel(state, σ0)
    else:
        fn = total_channel

    if in_place:
        settings["channel"] = fn
    else:
        return fn
