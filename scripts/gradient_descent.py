#!/usr/bin/env python3
import sys, os

# add directory to import path
sys.path.insert(1, os.path.join(sys.path[0], "../python_source"))
# sys.path.insert(1, "/central/groups/RelativityTheory/jwgardne/pleasantpheasant/python_source")

import pickle
import math
import numpy as np
from scipy.optimize import minimize, NonlinearConstraint

from operators import *
from states import *
from loss_channels import *
from encoding_channels import *
from fisher_information import *

# -----------------------
# ---- user settings ----
# -----------------------
# use task_id as the separation amount k
k = int(sys.argv[1])
# N = 100
# N = 500
# N = 650
# N = 800
N = 900
# N = 1000
# num_peaks = 10
# num_peaks = 15
# num_peaks = 18
# num_peaks = 20
# num_peaks = 24
# num_peaks = 25
# num_peaks = 30
num_peaks = N // k + 1 if N % k != 0 else N // k  # maximal number of peaks
# loss_tol = 1e-50
# loss_tol = 1e-100
# loss_tol = 1e-150
kmax = 200
with_phases = True
# deterministic = True
deterministic = False

# specific options to gradient descent
maxiter = 1000
# print_every = 50
# print_every = 10
print_every = 5

options = dict(
    k=k,
    kmax=kmax,
    num_peaks=num_peaks,
    with_phases=with_phases,
    maxiter=maxiter,
    print_every=print_every,
    deterministic=deterministic,
)
# -----------------------
# -----------------------

if k * num_peaks > N - 10:
    # reduce num_peaks if necessary for N, k
    num_peaks = N // k + 1
    if N % k == 0:
        num_peaks -= 1
else:
    # reduce N to save on compute time given k, num_peaks
    N = min(N, k * num_peaks + 10)

settings = dict(
    N=N,
    σ=1e-3,
    δσ=1e-5,
    η=1e-1,
    # η=1e-3,
    small_signal=True,
    cached_losses=True,
)
print(
    f"State comprises {num_peaks} peaks separated by k = {k} from |{0}> to |{(num_peaks - 1) * k}> inside the truncated Hilbert space of dimension N = {N}.\nOptimiser set for {maxiter} maximum iterations with a loss tolerance of {kmax} for η = {settings['η']} and with complex phases = {with_phases}. Deterministic displacement = {deterministic}."
)

# TODO: see if the memory requirement need to increase for the cache
cache = construct_M_k_cache(N, settings["η"], kmax)
print("Calculated cached loss Kraus operators.")
settings["cache"] = cache
construct_total_channel(settings)

kets = np.array([fock_ket(N, n * k) for n in range(num_peaks)])


def construct_ket(ps):
    """Return normalised ket from probabilities of all peaks, including \ket0"""
    ps /= np.sum(ps)
    return normalise(N, np.sum(np.sqrt(ps)[:, np.newaxis] * kets, axis=0))


def construct_ket_with_phases(ket_parts_peaks):
    """Return normalised ket from real 2*m vector of ket parts between -1 and 1 for m peaks"""
    # probability amplitudes for the peaks, real_to_complex still works even with the gaps
    ket_peaks = real_to_complex(ket_parts_peaks)
    # renormalise, can't we just use np.abs here?
    norm_peaks = (ket_peaks.conj() @ ket_peaks).real
    ket_peaks /= norm_peaks**0.5
    # construct ket
    return normalise(N, np.sum(ket_peaks[:, np.newaxis] * kets, axis=0))


def callback(x, intermediate_result):
    # TODO: save the intermediate kets/pickled options instead of printing to make checking the QFI easier. Override it on subsequent iterations.
    # it appears that x and intermediate_result.x agree

    if intermediate_result.nit % print_every == 0:
        print(
            f"Iteration = {intermediate_result.nit}, QFI = {-intermediate_result.fun:.3f}, x = {x}"
        )

        # f"N={N},k={k},peaks={num_peaks},kmax={kmax},with_phases={int(with_phases)}"
        label = f"{os.environ['SLURM_JOB_ID']}_{os.environ['SLURM_ARRAY_TASK_ID']}"

        filename = f"./data/gradient_descent/{label}.npy"
        ket = construct_ket_with_phases(x) if with_phases else construct_ket(x)
        np.save(filename, ket)

        filename = f"./data/gradient_descent/{label}.pkl"
        full_results = dict(
            niter=intermediate_result.nit,
            ket=ket,
            results=intermediate_result,
            settings={k: v for k, v in settings.items() if k != "channel"},
            options=options,
        )
        with open(filename, "wb") as file:
            pickle.dump(full_results, file)


if with_phases:

    if not deterministic:

        def objective(ket_parts_peaks):
            return -QFI_finite_difference(
                construct_ket_with_phases(ket_parts_peaks), settings
            )

    else:

        def objective(ket_parts_peaks):
            ket = construct_ket_with_phases(ket_parts_peaks)
            ρ_after_input_loss = loss_kraus_cached(N, ket, cache)
            return -QFI_mixed_state_unitary_encoding(N, ρ_after_input_loss, p_quad(N))

    # uniform starting probabilities with real positive phases initially, TODO: try random phases?
    initial_guess = np.zeros((2 * num_peaks,))
    initial_guess[0::2] = np.full((num_peaks,), 1 / num_peaks**0.5)
    # like construct_bounds, but just for peaks
    bounds = [(-1, 1) for _ in range(2 * num_peaks)]
    # TODO: try callback_minimize from brute_....py
else:

    def objective(ps):
        return -QFI_finite_difference(construct_ket(ps), settings)

    # initial_guess = np.full((num_peaks - 1,), 1 / num_peaks)
    initial_guess = np.full((num_peaks,), 1 / num_peaks)
    bounds = [(0, 1) for _ in range(num_peaks)]
print(f"Initial guess: QFI = {-objective(initial_guess):.3f}, x = {initial_guess}")
# constraint tolerance to prevent overstepping and math domain errors, TODO: determine the step size of the numerical derivative and set this accordingly
# constraint_tol = 1e-10
# constraint_tol = 0
# bounds = [(0, 1 - constraint_tol) for _ in range(num_peaks - 1)]
# TODO: still seeing invalid values, is this because of the two-sided FDM? Add in a tolerance below 1 to keep to allow for FDM?
# constraints = [NonlinearConstraint(sum, 1 - constraint_tol, 1 + constraint_tol, keep_feasible=True)]
# TODO: determine why a linear constraint is less respected by the algorithm
# constraints = [LinearConstraint([1 for _ in range(num_ps)], 0, 1, keep_feasible=True)]

results = minimize(
    objective,
    initial_guess,
    bounds=bounds,
    # constraints=constraints,
    method="trust-constr",
    # tol=1e-4,
    options=dict(maxiter=maxiter),
    callback=callback,
)

print(f"Final guess: QFI = {-results.fun:.3f}, x = {results.x}")
print(f"Full results:\n{results}")

label = f"N={N},k={k},peaks={num_peaks},it={min(results.niter, maxiter)},QFI={-results.fun:.1f},kmax={kmax},with_phases={int(with_phases)}"

# save best ket, using absolute paths to avoid confusion
# file = f"/central/groups/RelativityTheory/jwgardne/pleasantpheasant/data/gradient_descent/ps-{label}.npy"
# TODO: upgrade to .h5 to save settings etc. along with final ket for easy loading/verification and shorter file names (just can name after the job id_task id.h5)
file = f"./data/gradient_descent/ket-{label}.npy"
ket = construct_ket_with_phases(results.x) if with_phases else construct_ket(results.x)
np.save(file, ket)

filename = f"./data/gradient_descent/{os.environ['SLURM_JOB_ID']}_{os.environ['SLURM_ARRAY_TASK_ID']}.pkl"
full_results = dict(
    ket=ket,
    # TODO: see if 'jac' needs to be omitted
    results=results,
    # channel is local function which is not pickleable
    settings={k: v for k, v in settings.items() if k != "channel"},
    options=options,
)
with open(filename, "wb") as file:
    pickle.dump(full_results, file)

# with open(filename, 'rb') as file:
#     full_results = pickle.load(file)
