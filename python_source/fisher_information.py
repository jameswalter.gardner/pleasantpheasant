from operators import *
from states import *
from loss_channels import *
from encoding_channels import *

# eigensystem formula
from QFImixed_finite import QFImixed_finite
from scipy.linalg import sqrtm

# from tqdm import tqdm
from tqdm.notebook import tqdm

# fidelity formula
# from QFImixed_finite_fast import QFImixed_finite


def cfi(probs1, probs2, δθ, debug=False):
    """Return CFI calculated via finite difference.

    Args:
        probs1 (Nmax-by-1): probability distribution
        probs2 (Nmax-by-1): infinitesimally different probability distribution
        δθ (float): step size for finite difference method
    """
    probs_mid = 0.5 * (probs1 + probs2)
    deriv = (probs2 - probs1) / δθ
    cfi_summand = deriv**2 / probs_mid
    Fc = np.nansum(cfi_summand)
    if not debug:
        return Fc
    else:
        return Fc, cfi_summand


def cfi_fock(ρ1, ρ2, δσ):
    """Return CFI wrt Fock basis calculated via finite difference.

    Args:
        ρ1 (Nmax-by-Nmax): density matrix
        ρ2 (Nmax-by-Nmax): infinitesimally different density matrix
        δσ (float): step size for finite difference method
    """
    Pn1 = ρ1.diagonal().real
    Pn2 = ρ2.diagonal().real
    return cfi(Pn1, Pn2, δσ)


def qfi(ρ1, ρ2, δθ):
    """Return QFI calculated via finite difference.

    Args:
        ρ1 (Nmax-by-Nmax): density matrix
        ρ2 (Nmax-by-Nmax): infinitesimally different density matrix
        δσ (float): step size for finite difference method
    """
    Fq = 1 / δθ**2 * QFImixed_finite(ρ1, ρ2).real
    return Fq


def FI_from_channel(ρ0, channel, θ, δθ, which_FI="QFI"):
    """Returns channel FI. TODO: write docstring manually since autofill broken will latex chrs"""
    # density matrices after channel
    ρ1 = channel(ρ0, θ)
    ρ2 = channel(ρ0, θ + δθ)

    # calculating FIs via the finite difference method
    args = (ρ1, ρ2, δθ)
    if which_FI == "both":
        Fc = cfi_fock(*args)
        Fq = qfi(*args)
        return (Fc, Fq)
    elif which_FI == "CFI":
        return cfi_fock(*args)
    elif which_FI == "QFI":
        return qfi(*args)


def FI_from_state(initial_state, settings, override_channel=False):
    """Returns FI for initial state given settings, including 'channel'."""
    ρ0 = ρ_from_state(initial_state)
    if override_channel or "channel" not in settings.keys():
        print("Constructing total channel from settings.")
        construct_total_channel(settings)
    return FI_from_channel(
        ρ0=ρ0,
        channel=settings["channel"],
        θ=settings["σ"],
        δθ=settings["δσ"],
        which_FI=settings["which_FI"],
    )


def reject_eigenvalues(λ_j, λ_k, tol=1e-15, chop=True):
    """Returns whether the evals should be rejected and the loop continue past them."""
    # drop imaginary values
    if abs(λ_j.imag) > tol or abs(λ_k.imag) > tol:
        raise ValueError(f"Eigenvalues are complex: λ_j={λ_j}, λ_k={λ_k}.")
    else:
        λ_j = λ_j.real
        λ_k = λ_k.real

    return (
        # drop singular values
        (λ_k + λ_j == 0)
        # drop numerically singular values
        or (chop and abs(λ_k.real + λ_j.real) < tol)
        # drop negative values
        or (λ_j.real < 0 or λ_k.real < 0)
    )


def QFI_from_deriv(ρ, ρ_dot):
    # ρ_dot is derivative (called deriv in sld()) calculated from Λ_dot or finite difference method
    # TODO: use the positive definiteness of rho to find them faster
    evals, evecs = np.linalg.eigh(ρ)

    FQ_element = np.zeros_like(ρ)

    # TODO: vectorise this, but respect the reject_eigevalues and nansum. There's probably code out there that already calculates FI fast.
    for j, (λ_j, ψ_j) in enumerate(zip(evals, evecs.T)):
        for k, (λ_k, ψ_k) in enumerate(zip(evals, evecs.T)):
            if reject_eigenvalues(λ_j, λ_k):
                continue

            # from Simon's QFImixed_finite.m
            FQ_element[j, k] = (
                4 * λ_j * np.abs(np.conj(ψ_k) @ ρ_dot @ ψ_j) ** 2 / (λ_k + λ_j) ** 2
            )

    FQ = np.nansum(FQ_element).real
    return FQ

    # ChatGPT's suggestion
    # Use np.nditer for nested iteration
    # TODO: check that this is extracting the evecs as columns correctly
    # https://stackoverflow.com/a/10148855
    # suggests using ``for ψ_j in evecs.T:''
    # with np.nditer([evals, evecs.T], op_flags=[["readonly"], ["readonly"]]) as it:
    #     for (λ_j, ψ_j) in it:
    #         print(λ_j, ψ_j)
    #         break
    #     for (λ_j, ψ_j), (λ_k, ψ_k) in it, it:
    #         if reject_eigenvalues(λ_j, λ_k):
    #             continue

    #         # Vectorized calculation
    #         # TODO: check that this works (I am doubtful since the quantity on the right is a scalar?)
    #         FQ_element += (
    #             4 * λ_j * np.abs(np.conj(ψ_k) @ ρ_dot @ ψ_j) ** 2 / (λ_k + λ_j) ** 2
    #         )


def QFI_mixed_state_unitary_encoding(N, state, H):
    """From PS+18 Eq 14 or DD Eq 87"""
    ρ0 = ρ_from_state(N, state)

    # TODO: test if the state is pure within tolerance like Simon does, and if so use the 4*var[H] formula
    if 1 - purity(ρ0) < 1e-5:
        raise ValueError("State is too pure.")

    evals, evecs = np.linalg.eigh(ρ0)

    Fq_element = np.zeros_like(ρ0)
    for j, (λ_j, ψ_j) in enumerate(zip(evals, evecs.T)):
        for k, (λ_k, ψ_k) in enumerate(zip(evals, evecs.T)):

            if reject_eigenvalues(λ_j, λ_k):
                continue

            Fq_element[j, k] = (
                2 * (λ_j - λ_k) ** 2 * np.abs(np.conj(ψ_k) @ H @ ψ_j) ** 2 / (λ_j + λ_k)
            )
    Fq = np.nansum(Fq_element).real
    return Fq


def QFI_mixed_state_unitary_encoding_Schneiter2020(N, state, H):
    """From Schneiter+2020"""
    ρ0 = ρ_from_state(N, state)
    evals, evecs = np.linalg.eigh(ρ0)

    Fq_1st_term = 0
    Fq_2nd_term_element = np.zeros_like(ρ0)
    for j, (λ_j, ψ_j) in enumerate(zip(evals, evecs.T)):
        Fq_1st_term += 4 * λ_j * variance(N, ψ_j, H)

        for k, (λ_k, ψ_k) in enumerate(zip(evals, evecs.T)):
            if j == k or reject_eigenvalues(λ_j, λ_k):
                continue

            Fq_2nd_term_element[j, k] = (
                -8 * λ_j * λ_k * np.abs(np.conj(ψ_k) @ H @ ψ_j) ** 2 / (λ_k + λ_j)
            )
    Fq_2nd_term = np.nansum(Fq_2nd_term_element).real

    Fq = Fq_1st_term + Fq_2nd_term
    return Fq


def finite_difference_ρ_and_ρ_dot(
    initial_state,
    settings,
    return_ρ1_ρ2_too=False,
):
    """Return the (midpoint) state after the channel and its derivative calculated via finite difference."""
    # initial state
    ρ0 = ρ_from_state(settings["N"], initial_state)

    # final states
    if "channel" not in settings.keys():
        print("Constructing total channel from settings.", flush=True)
        construct_total_channel(settings)
    # ρ1 = settings["channel"](ρ0, settings["σ"])
    # ρ2 = settings["channel"](ρ0, settings["σ"] + settings["δσ"])
    # sampling left/right to make the midpoint at the given σ, changes at the 5th SF
    σ_minus = settings["σ"] - settings["δσ"] / 2
    σ_plus = settings["σ"] + settings["δσ"] / 2
    ρ1 = settings["channel"](ρ0, σ_minus)
    ρ2 = settings["channel"](ρ0, σ_plus)

    # midpoint of the final states, TODO: compare this to settings["channel"](ρ0, settings["σ"])
    ρ = normalise(settings["N"], 0.5 * (ρ1 + ρ2))

    # derivative at midpoint
    δρ = ρ2 - ρ1
    ρ_dot = δρ / settings["δσ"]
    if not return_ρ1_ρ2_too:
        return ρ, ρ_dot
    else:
        return ρ, ρ_dot, ρ1, ρ2


def QFI_finite_difference(initial_state, settings):
    """Return the QFI calculated via the finite difference method."""
    ρ, ρ_dot = finite_difference_ρ_and_ρ_dot(initial_state, settings)
    return QFI_from_deriv(ρ, ρ_dot)


def CFI_Fock_finite_difference(initial_state, settings):
    _, _, ρ1, ρ2 = finite_difference_ρ_and_ρ_dot(
        initial_state, settings, return_ρ1_ρ2_too=True
    )
    return cfi_fock(ρ1, ρ2, settings["δσ"])


def vectorise(M):
    # F is fortran ordering, i.e. by column, to achieve column-stacking
    return M.flatten(order="F")


def unvectorise(N, v):
    # undo vectorise()
    return v.reshape((N, N), order="F")


def apply_LSO(N, LSO, ρ):
    return unvectorise(N, LSO @ vectorise(ρ))


def QFI_from_LSO(N, state, Λ_LSO, Λ_dot_LSO):
    # TODO: write a similar version using Λ_dot without LSOs
    ρ0 = ρ_from_state(N, state)
    ρ = apply_LSO(N, Λ_LSO, ρ0)
    ρ_dot = apply_LSO(N, Λ_dot_LSO, ρ0)
    return QFI_from_deriv(ρ, ρ_dot)


def QFI_from_Λ_dot(state, settings):
    """Return the QFI calculated from the known channel derivative at the given σ without requiring δσ."""
    Λ = construct_total_channel(
        settings, renormalise=True, in_place=False, σ0=settings["σ"]
    )
    Λdot = construct_total_channel(
        settings, use_pdot=True, renormalise=False, in_place=False, σ0=settings["σ"]
    )
    ρ0 = ρ_from_state(settings["N"], state)
    ρ = Λ(ρ0)
    ρ_dot = Λdot(ρ0)
    return QFI_from_deriv(ρ, ρ_dot)


def solve_Lyapunov_equation(A, B, chop=True, tqdm_bar=False):
    """Find X that solves A = (X @ B + B @ X) / 2."""
    evals, evecs = np.linalg.eigh(B)
    X = np.zeros_like(B)
    if tqdm_bar:
        loop = tqdm(zip(evals, evecs.T), total=len(evals), desc='Solving the Lyapunov equation')
    else:
        loop = zip(evals, evecs.T)
    for λ_j, ψ_j in loop:
        for λ_k, ψ_k in zip(evals, evecs.T):
            if reject_eigenvalues(λ_j, λ_k, chop=chop):
                continue
            X += (
                2 / (λ_k + λ_j) * (np.conj(ψ_k) @ A @ ψ_j) * np.outer(ψ_k, np.conj(ψ_j))
            )
    return X


def sld_from_deriv(ρ, ρ_dot, chop=True):
    # Paris09, Eq 12 or DnD+15, Eq 78
    return solve_Lyapunov_equation(A=ρ_dot, B=ρ, chop=chop)


def QFI_via_SLD(ρ, ρ_dot, chop=True):
    L = sld_from_deriv(ρ=ρ, ρ_dot=ρ_dot, chop=chop)
    return np.trace(ρ @ L @ L).real


def Bayesian_optimal_estimator(ρ_bar, ρ_bar_dot, chop=True, tqdm_bar=False):
    """$L=\inth{x}\tilde\om_x\proj{x}$ is the optimal estimator for the Bayesian minimum BMSE from Macieszczak+14 Eq.5, i.e. the average SLD."""
    return solve_Lyapunov_equation(A=ρ_bar_dot, B=ρ_bar, chop=chop, tqdm_bar=tqdm_bar)


def Bayesian_minimum_BMSE_via_optimal_estimator(σ_prior, ρ_bar, ρ_bar_dot, chop=True, tqdm_bar=False):
    L = Bayesian_optimal_estimator(ρ_bar=ρ_bar, ρ_bar_dot=ρ_bar_dot, chop=chop, tqdm_bar=tqdm_bar)
    # print(f'Decrease in BMSE from prior: {np.trace(ρ_bar @ L @ L).real:.3g}')
    return σ_prior**2 - np.trace(ρ_bar @ L @ L).real


def Bayesian_minimum_BMSE(σ_prior, ρ_bar, ρ_bar_dot, chop=True, tqdm_bar=False):
    # TODO: check that this gives the same as calculating L above using Bayesian_minimum_BMSE_via_optimal_estimator
    # Right now this is not working, so it needs to be fixed!
    evals, evecs = np.linalg.eigh(ρ_bar)
    # FQ is the QFI-like quantity here: Tr[ρ_bar @ L ** 2]
    FQ_element = np.zeros_like(ρ_bar)
    if tqdm_bar:
        loop = enumerate(tqdm(zip(evals, evecs.T), total=len(evals), desc='Calculating the MBMSE'))
    else:
        loop = enumerate(zip(evals, evecs.T))
    for j, (λ_j, ψ_j) in loop:
        for k, (λ_k, ψ_k) in enumerate(zip(evals, evecs.T)):
            if reject_eigenvalues(λ_j, λ_k, chop=chop):
                continue
            # # TODO: test whether Simon's formula for the QFI works here and fixes the issue above?
            FQ_element[j, k] = (
                4 * λ_j / (λ_k + λ_j) ** 2 * np.abs(np.conj(ψ_k) @ ρ_bar_dot @ ψ_j) ** 2
            )
            # FQ_element[j, k] = 2 / (λ_k + λ_j) * np.abs(np.conj(ψ_k) @ ρ_bar_dot @ ψ_j) ** 2
    FQ = np.nansum(FQ_element).real
    # print(f'Decrease in BMSE from prior: {FQ:.3g}')
    return σ_prior**2 - FQ


def sld(state, settings, chop=True, return_dict=False):
    if not chop:
        print("Are you sure that you do not want to chop the SLD?")

    if not return_dict:
        ρ, ρ_dot = finite_difference_ρ_and_ρ_dot(state, settings)
    else:
        ρ, ρ_dot, ρ1, ρ2 = finite_difference_ρ_and_ρ_dot(
            state, settings, return_ρ1_ρ2_too=True
        )

    # The normalized (unit “length”) eigenvectors, such that the column eigenvectors[:,i] is the eigenvector corresponding to the eigenvalue eigenvalues[i].
    evals, evecs = np.linalg.eigh(ρ)

    SLD = np.zeros_like(ρ)
    if return_dict:
        SLD_components = np.zeros_like(ρ)

    # TODO: vectorise this
    for j, (λ_j, ψ_j) in enumerate(zip(evals, evecs.T)):
        for k, (λ_k, ψ_k) in enumerate(zip(evals, evecs.T)):

            if reject_eigenvalues(λ_j, λ_k, chop=chop):
                continue

            # Paris09, Eq 12 or DnD+15, Eq 78
            # This 2/(p1+p2) is different to the 4p1/(p1+p2)^2 used above but they must be the same
            # TODO: find the proof or show yourself that these are the same
            component = 2 * (np.conj(ψ_k) @ ρ_dot @ ψ_j) / (λ_k + λ_j)
            SLD += component * np.outer(ψ_k, np.conj(ψ_j))
            if return_dict:
                SLD_components[j, k] = component

    if not return_dict:
        return SLD
    else:
        return dict(
            sld=SLD,
            sld_components=SLD_components,
            ρ=ρ,
            ρ_dot=ρ_dot,
            evals=evals,
            evecs=evecs,
            ρ1=ρ1,
            ρ2=ρ2,
        )


def cfi_sld(state, settings, debug=True, **kwargs):
    """Returns the CFI wrt the basis of the SLD for the given state and settings, including channel."""
    # TODO: generalise to cfi_X and final_states
    ρ0 = ρ_from_state(settings["N"], state)
    SLD_dict = sld(ρ0, settings, return_dict=True, **kwargs)
    SLD = SLD_dict["sld"]
    ρ1 = SLD_dict["ρ1"]
    ρ2 = SLD_dict["ρ2"]

    # TODO: check that the evecs are orthogonal since SLD should be Hermitian (check that too)
    # TODO: check that the evals are nondegenerate such that the measurements can be distinguished
    _, evecs = np.linalg.eigh(SLD)

    probs1 = np.zeros(settings["N"])
    probs2 = np.zeros(settings["N"])
    # TODO: use matrix algebra to calculate this faster
    for j in range(settings["N"]):
        ψ_j = evecs[:, j]
        prob1 = np.conj(ψ_j) @ ρ1 @ ψ_j
        prob2 = np.conj(ψ_j) @ ρ2 @ ψ_j

        tol = 1e-15
        if abs(prob1.imag) > tol or abs(prob2.imag) > tol:
            raise ValueError("Probabilities are complex.")
        else:
            prob1 = prob1.real
            prob2 = prob2.real

        probs1[j] = prob1
        probs2[j] = prob2

    Fc = cfi(probs1, probs2, settings["δσ"])
    if not debug:
        return Fc
    else:
        return Fc, probs1, probs2


def fidelity(N, state1, state2):
    """Bures quantum fidelity between states ρ1 and ρ2, sometimes given as the square root of this quantity, but apparantely this convention is more common, see https://en.wikipedia.org/wiki/Fidelity_of_quantum_states."""
    ρ1 = ρ_from_state(N, state1)
    ρ2 = ρ_from_state(N, state2)
    return (np.trace(sqrtm(sqrtm(ρ2) @ ρ1 @ sqrtm(ρ2)))).real ** 2
