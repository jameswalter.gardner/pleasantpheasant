(* ::Package:: *)

(* ::Text:: *)
(*Gaussian QFI/QFIM*)
(*Using the single-mode formulae from Pinel+13*)


(* ::Input:: *)
(*(*NB: Need to compile Initialisation cells as package:*)
(*https://mathematica.stackexchange.com/a/1370*)*)


(* ::Input::Initialization:: *)
Clear[Gaussian\[LetterSpace]QFI];
Gaussian\[LetterSpace]QFI[\[Mu]vec_,\[CapitalSigma]_,\[Theta]_,print_:False]:=(
If[Dimensions[\[CapitalSigma]]!={2,2},Print["Error: Covariance matrix needs to be 2-by-2 or use multi-mode Monras13 formula."]];
(*purity*)
\[Gamma] =1/Sqrt[Det[2\[CapitalSigma]]]//FullSimplify;
QFI = (
Tr[MatrixPower[Inverse[\[CapitalSigma]] . D[\[CapitalSigma],\[Theta]],2]]/(2(1+\[Gamma]^2))
+If[D[\[Gamma],\[Theta]]===0,0,(2D[\[Gamma],\[Theta]]^2)/(1-\[Gamma]^4)]
+Block[{expr=(Transpose[D[\[Mu]vec,\[Theta]]] . Inverse[\[CapitalSigma]] . D[\[Mu]vec,\[Theta]])},If[expr===0,0,expr[[1,1]]]]
//FullSimplify//Factor//FullSimplify
);
If[print,
Print["Purity: ",\[Gamma]];
Print["QFI wrt ",\[Theta],": ",QFI];
];
Return[QFI];
)

Clear[Gaussian\[LetterSpace]QFIM];
Gaussian\[LetterSpace]QFIM[\[Mu]vec_,\[CapitalSigma]_,\[Theta]vec_,print_:False]:=(
If[Dimensions[\[CapitalSigma]]!={2,2},Print["Error: Covariance matrix needs to be 2-by-2 or use multi-mode Monras13 formula."]];
(*purity*)
\[Gamma]=1/Sqrt[Det[2\[CapitalSigma]]]//FullSimplify;
QFIM=Table[(
1/(2(1+\[Gamma]^2)) Tr[Inverse[\[CapitalSigma]] . D[\[CapitalSigma],\[Theta]vec[[j]]] . Inverse[\[CapitalSigma]] . D[\[CapitalSigma],\[Theta]vec[[k]]]]
+2/(1-\[Gamma]^4) D[\[Gamma],\[Theta]vec[[j]]]D[\[Gamma],\[Theta]vec[[k]]]
+(Transpose[D[\[Mu]vec,\[Theta]vec[[j]]]] . Inverse[\[CapitalSigma]] . D[\[Mu]vec,\[Theta]vec[[k]]])[[1,1]]
),
{j,1,Length[\[Theta]vec]},
{k,1,Length[\[Theta]vec]}
]//FullSimplify//Factor//FullSimplify;
If[print,
Print["Purity: ",\[Gamma]];
Print["QFIM wrt ",\[Theta]vec//MatrixForm,": ",QFIM//MatrixForm];
];
Return[QFIM];
)
convert\[LetterSpace]matrix\[LetterSpace]to\[LetterSpace]wrt\[LetterSpace]variance[M_]:=({
 {M[[1,1]], 1/(2\[Sigma]) M[[1,2]]},
 {1/(2\[Sigma]) M[[2,1]], 1/(4\[Sigma]^2) M[[2,2]]}
})
