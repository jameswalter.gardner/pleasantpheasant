#!/usr/bin/env python3

# https://gist.github.com/kaspermunch/64e11cd21e3953295e149e75bee51733
import os
from multiprocessing import Pool
import numpy as np


# function you want to run in parallel:
def myfunction(a, b):
    return a + b**2


# list of tuples to serve as arguments to function:
# inputs = [(1, 2), (9, 11), (6, 2)]
inputs = np.random.random((1000, 2))

# number of cores you have allocated for your slurm task:
number_of_cores = int(os.environ["SLURM_CPUS_PER_TASK"])
print(f"{number_of_cores} cores are available.")

# multiprocssing pool to distribute tasks to:
with Pool(number_of_cores) as pool:
    # distribute computations and collect results:
    results = pool.starmap(myfunction, inputs)

print(np.all(results == inputs[:, 0] + inputs[:, 1] ** 2))
print(results)
