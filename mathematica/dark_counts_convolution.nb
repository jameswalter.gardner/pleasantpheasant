(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     21559,        588]
NotebookOptionsPosition[     18421,        531]
NotebookOutlinePosition[     18789,        547]
CellTagsIndexPosition[     18746,        544]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Measurement noise from convolution", "Text",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.},
 CellChangeTimes->{{3.9210833484757223`*^9, 3.9210833604324903`*^9}, 
   3.921152705874423*^9, {3.9212412822590017`*^9, 
   3.921241316882413*^9}},ExpressionUUID->"9b205085-fa49-4540-8e06-\
eb6dde7a37d1"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"p1", "[", "n_", "]"}], ":=", 
  RowBox[{"If", "[", 
   RowBox[{
    RowBox[{"n", "\[Equal]", "0"}], ",", 
    RowBox[{"1", "-", 
     SuperscriptBox["\[Xi]\[Sigma]", "2"]}], ",", 
    RowBox[{"If", "[", 
     RowBox[{
      RowBox[{"n", "\[Equal]", "1"}], ",", 
      SuperscriptBox["\[Xi]\[Sigma]", "2"], ",", "0"}], "]"}]}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"p", "[", "n_", "]"}], ":=", 
  RowBox[{
   FractionBox["1", 
    RowBox[{"nbar", "+", "1"}]], 
   SuperscriptBox[
    RowBox[{"(", 
     FractionBox["nbar", 
      RowBox[{"nbar", "+", "1"}]], ")"}], "n"]}]}], "\[IndentingNewLine]", 
 RowBox[{"Sum", "[", 
  RowBox[{
   RowBox[{"p", "[", "n", "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"n", ",", "0", ",", "\[Infinity]"}], "}"}]}], "]"}]}], "Input",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.},
 CellChangeTimes->{{3.921083362289706*^9, 3.921083395030747*^9}, 
   3.921152705875351*^9, {3.921237092456058*^9, 3.921237120867669*^9}},
 CellLabel->"In[1]:=",ExpressionUUID->"f8cbe6ce-b72f-4cf0-846a-48d0cc06d29b"],

Cell[BoxData["1"], "Output",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.},
 CellChangeTimes->{{3.921083381381887*^9, 3.921083395317081*^9}, 
   3.92115270587611*^9, 3.921236717879787*^9, 3.921237123486327*^9, 
   3.921259060477386*^9},
 CellLabel->"Out[3]=",ExpressionUUID->"a7dd3247-6c97-4cfd-ba0d-5cd8a30715dd"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"p", "[", "k", "]"}], "/.", 
   RowBox[{"nbar", "\[Rule]", 
    RowBox[{
     SuperscriptBox["\[Sigma]", "2"], "-", 
     FractionBox["1", "2"]}]}]}], "//", "FullSimplify"}]], "Input",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.},
 CellChangeTimes->{{3.9210833965998297`*^9, 3.9210834071402893`*^9}, {
   3.921083488546445*^9, 3.92108348884909*^9}, 3.921152705876787*^9},
 CellLabel->"In[9]:=",ExpressionUUID->"eb3f8e4a-217a-42b5-b107-635e80c80336"],

Cell[BoxData[
 RowBox[{"2", " ", 
  SuperscriptBox[
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "1"}], "+", 
     RowBox[{"2", " ", 
      SuperscriptBox["\[Sigma]", "2"]}]}], ")"}], "k"], " ", 
  SuperscriptBox[
   RowBox[{"(", 
    RowBox[{"1", "+", 
     RowBox[{"2", " ", 
      SuperscriptBox["\[Sigma]", "2"]}]}], ")"}], 
   RowBox[{
    RowBox[{"-", "1"}], "-", "k"}]]}]], "Output",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.},
 CellChangeTimes->{3.921084009565317*^9, 3.921152705877447*^9},
 CellLabel->"Out[9]=",ExpressionUUID->"74a35ebe-cab0-4946-a950-96ee5a536584"],

Cell[BoxData[{
 RowBox[{"Clear", "[", "unswapped", "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"unswapped", "[", "n_", "]"}], ":=", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"p1", "[", "1", "]"}], 
     RowBox[{"p", "[", 
      RowBox[{"n", "-", "1"}], "]"}]}], "+", 
    RowBox[{
     RowBox[{"p1", "[", "0", "]"}], 
     RowBox[{"p", "[", "n", "]"}]}]}], "//", 
   "FullSimplify"}]}], "\[IndentingNewLine]", 
 RowBox[{"unswapped", "[", "n", "]"}]}], "Input",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.},
 CellChangeTimes->{{3.921236725817709*^9, 3.9212367514471207`*^9}, {
  3.921237125831781*^9, 3.921237138860373*^9}, {3.921259117334848*^9, 
  3.9212591779526443`*^9}},
 CellLabel->"In[14]:=",ExpressionUUID->"eb1142d9-c460-4c9f-84f0-5316e55f4621"],

Cell[BoxData[
 RowBox[{
  SuperscriptBox["nbar", 
   RowBox[{
    RowBox[{"-", "1"}], "+", "n"}]], " ", 
  SuperscriptBox[
   RowBox[{"(", 
    RowBox[{"1", "+", "nbar"}], ")"}], 
   RowBox[{
    RowBox[{"-", "1"}], "-", "n"}]], " ", 
  RowBox[{"(", 
   RowBox[{"nbar", "+", 
    SuperscriptBox["\[Xi]\[Sigma]", "2"]}], ")"}]}]], "Output",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.},
 CellChangeTimes->{{3.9212367396816063`*^9, 3.9212367520587273`*^9}, {
   3.9212371337429132`*^9, 3.921237138861168*^9}, 3.921259078349792*^9, {
   3.921259118746737*^9, 3.921259178287642*^9}},
 CellLabel->"Out[16]=",ExpressionUUID->"057db5c2-7db8-4dee-b329-f35e08673859"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"{", 
   RowBox[{
    RowBox[{
     RowBox[{"p1", "[", "0", "]"}], 
     RowBox[{"p", "[", "0", "]"}]}], ",", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"unswapped", "[", "n", "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"n", ",", "1", ",", "5"}], "}"}]}], "]"}]}], "}"}], "//", 
  "Flatten"}]], "Input",
 CellChangeTimes->{{3.921259180320196*^9, 3.921259193497507*^9}},
 CellLabel->"In[18]:=",ExpressionUUID->"9fe0232d-1e81-4246-8733-5709c1fb34b4"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox[
    RowBox[{"1", "-", 
     SuperscriptBox["\[Xi]\[Sigma]", "2"]}], 
    RowBox[{"1", "+", "nbar"}]], ",", 
   FractionBox[
    RowBox[{"nbar", "+", 
     SuperscriptBox["\[Xi]\[Sigma]", "2"]}], 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{"1", "+", "nbar"}], ")"}], "2"]], ",", 
   FractionBox[
    RowBox[{"nbar", " ", 
     RowBox[{"(", 
      RowBox[{"nbar", "+", 
       SuperscriptBox["\[Xi]\[Sigma]", "2"]}], ")"}]}], 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{"1", "+", "nbar"}], ")"}], "3"]], ",", 
   FractionBox[
    RowBox[{
     SuperscriptBox["nbar", "2"], " ", 
     RowBox[{"(", 
      RowBox[{"nbar", "+", 
       SuperscriptBox["\[Xi]\[Sigma]", "2"]}], ")"}]}], 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{"1", "+", "nbar"}], ")"}], "4"]], ",", 
   FractionBox[
    RowBox[{
     SuperscriptBox["nbar", "3"], " ", 
     RowBox[{"(", 
      RowBox[{"nbar", "+", 
       SuperscriptBox["\[Xi]\[Sigma]", "2"]}], ")"}]}], 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{"1", "+", "nbar"}], ")"}], "5"]], ",", 
   FractionBox[
    RowBox[{
     SuperscriptBox["nbar", "4"], " ", 
     RowBox[{"(", 
      RowBox[{"nbar", "+", 
       SuperscriptBox["\[Xi]\[Sigma]", "2"]}], ")"}]}], 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{"1", "+", "nbar"}], ")"}], "6"]]}], "}"}]], "Output",
 CellChangeTimes->{{3.9212591692615147`*^9, 3.921259193813856*^9}},
 CellLabel->"Out[18]=",ExpressionUUID->"31dc8bce-62b8-4bc6-9910-278ba9991c77"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", "CFI", "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"p0", "=", 
     RowBox[{
      RowBox[{"p1", "[", "0", "]"}], 
      RowBox[{"p", "[", "0", "]"}]}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{"Fc", " ", "=", " ", 
    RowBox[{
     RowBox[{
      FractionBox[
       SuperscriptBox[
        RowBox[{"D", "[", 
         RowBox[{
          RowBox[{"p0", "/.", 
           RowBox[{
            SuperscriptBox["\[Xi]\[Sigma]", "2"], "\[Rule]", 
            RowBox[{"\[Xi]", " ", 
             SuperscriptBox["\[Sigma]", "2"]}]}]}], ",", "\[Sigma]"}], "]"}], 
        "2"], 
       RowBox[{"p0", "/.", 
        RowBox[{
         SuperscriptBox["\[Xi]\[Sigma]", "2"], "\[Rule]", 
         RowBox[{"\[Xi]", " ", 
          SuperscriptBox["\[Sigma]", "2"]}]}]}]], "+", 
      RowBox[{"Sum", "[", 
       RowBox[{
        FractionBox[
         SuperscriptBox[
          RowBox[{"D", "[", 
           RowBox[{
            RowBox[{
             RowBox[{"unswapped", "[", "n", "]"}], "/.", 
             RowBox[{
              SuperscriptBox["\[Xi]\[Sigma]", "2"], "\[Rule]", 
              RowBox[{"\[Xi]", " ", 
               SuperscriptBox["\[Sigma]", "2"]}]}]}], ",", "\[Sigma]"}], 
           "]"}], "2"], 
         RowBox[{
          RowBox[{"unswapped", "[", "n", "]"}], "/.", 
          RowBox[{
           SuperscriptBox["\[Xi]\[Sigma]", "2"], "\[Rule]", 
           RowBox[{"\[Xi]", " ", 
            SuperscriptBox["\[Sigma]", "2"]}]}]}]], ",", 
        RowBox[{"{", 
         RowBox[{"n", ",", "1", ",", "\[Infinity]"}], "}"}]}], "]"}]}], "//", 
     "FullSimplify"}]}]}]}]], "Input",
 CellChangeTimes->{{3.921259107022414*^9, 3.921259112487811*^9}, {
  3.9212591603656683`*^9, 3.921259165761801*^9}, {3.921259215990535*^9, 
  3.9212592553037024`*^9}},
 CellLabel->"In[21]:=",ExpressionUUID->"14470b68-1df5-4f20-8af3-57b851df5cb1"],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox[
   RowBox[{"4", " ", 
    SuperscriptBox["\[Xi]", "2"], " ", 
    SuperscriptBox["\[Sigma]", "2"]}], 
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "1"}], "+", 
      RowBox[{"\[Xi]", " ", 
       SuperscriptBox["\[Sigma]", "2"]}]}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"nbar", "+", 
      RowBox[{"\[Xi]", " ", 
       SuperscriptBox["\[Sigma]", "2"]}]}], ")"}]}]]}]], "Output",
 CellChangeTimes->{{3.921259251951756*^9, 3.921259255666707*^9}},
 CellLabel->"Out[22]=",ExpressionUUID->"97195331-6fc6-4958-9291-d438733732fe"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{"swap", " ", "case"}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"p1", "[", "0", "]"}], 
     RowBox[{"p", "[", "n", "]"}]}], "+", 
    RowBox[{
     RowBox[{"p1", "[", "1", "]"}], 
     RowBox[{"p", "[", 
      RowBox[{"n", "-", "k"}], "]"}]}]}], "\[IndentingNewLine]", 
   RowBox[{"%", "//", "FullSimplify"}]}]}]], "Input",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.},
 CellChangeTimes->{{3.921237304754404*^9, 3.921237341369504*^9}, 
   3.9212412955839777`*^9},
 CellLabel->"In[23]:=",ExpressionUUID->"a2b95ef0-fdaf-4138-a820-3b2c95b4ddbe"],

Cell[BoxData[
 RowBox[{
  FractionBox[
   RowBox[{
    SuperscriptBox[
     RowBox[{"(", 
      FractionBox["nbar", 
       RowBox[{"1", "+", "nbar"}]], ")"}], 
     RowBox[{
      RowBox[{"-", "k"}], "+", "n"}]], " ", 
    SuperscriptBox["\[Xi]\[Sigma]", "2"]}], 
   RowBox[{"1", "+", "nbar"}]], "+", 
  FractionBox[
   RowBox[{
    SuperscriptBox[
     RowBox[{"(", 
      FractionBox["nbar", 
       RowBox[{"1", "+", "nbar"}]], ")"}], "n"], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", 
      SuperscriptBox["\[Xi]\[Sigma]", "2"]}], ")"}]}], 
   RowBox[{"1", "+", "nbar"}]]}]], "Output",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.},
 CellChangeTimes->{3.921237310861101*^9, 3.921237344898094*^9, 
  3.9212407429401817`*^9, 3.9212412955848494`*^9},
 CellLabel->"Out[23]=",ExpressionUUID->"45dfc52c-1d34-4c7a-945b-308ff728574b"],

Cell[BoxData[
 RowBox[{
  SuperscriptBox["nbar", "n"], " ", 
  SuperscriptBox[
   RowBox[{"(", 
    RowBox[{"1", "+", "nbar"}], ")"}], 
   RowBox[{
    RowBox[{"-", "1"}], "-", "n"}]], " ", 
  RowBox[{"(", 
   RowBox[{"1", "+", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "1"}], "+", 
       RowBox[{
        SuperscriptBox["nbar", 
         RowBox[{"-", "k"}]], " ", 
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{"1", "+", "nbar"}], ")"}], "k"]}]}], ")"}], " ", 
     SuperscriptBox["\[Xi]\[Sigma]", "2"]}]}], ")"}]}]], "Output",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.},
 CellChangeTimes->{3.921237310861101*^9, 3.921237344898094*^9, 
  3.921240742993622*^9, 3.92124129558578*^9},
 CellLabel->"Out[24]=",ExpressionUUID->"b3fd5456-61ac-4ce8-aeaa-e70c55e8a07a"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"prob", " ", "that", " ", "X"}], " ", "+", " ", 
    RowBox[{
     RowBox[{"Y", " ", "\\", "geq"}], " ", "k"}]}], "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"probGeqk", "=", 
    RowBox[{
     RowBox[{"1", "-", 
      RowBox[{
       FractionBox[
        RowBox[{"1", "-", 
         SuperscriptBox["\[Xi]\[Sigma]", "2"]}], 
        RowBox[{"nbar", "+", "1"}]], 
       RowBox[{"Sum", "[", 
        RowBox[{
         SuperscriptBox[
          RowBox[{"(", 
           FractionBox["nbar", 
            RowBox[{"nbar", "+", "1"}]], ")"}], "n"], ",", 
         RowBox[{"{", 
          RowBox[{"n", ",", "0", ",", 
           RowBox[{"k", "-", "1"}]}], "}"}]}], "]"}]}]}], "//", 
     RowBox[{
      RowBox[{"Collect", "[", 
       RowBox[{"#", ",", 
        SuperscriptBox["\[Xi]\[Sigma]", "2"], ",", "FullSimplify"}], "]"}], 
      "&"}]}]}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"probGeqk", "==", 
     RowBox[{"Sum", "[", 
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{"p1", "[", "0", "]"}], 
         RowBox[{"p", "[", "n", "]"}]}], "+", 
        RowBox[{
         RowBox[{"p1", "[", "1", "]"}], 
         RowBox[{"p", "[", 
          RowBox[{"n", "-", "k"}], "]"}]}]}], ",", 
       RowBox[{"{", 
        RowBox[{"n", ",", "k", ",", "\[Infinity]"}], "}"}]}], "]"}]}], "//", 
    "FullSimplify"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"probGeqk", "/.", 
     RowBox[{"k", "\[Rule]", "1"}]}], "//", "FullSimplify"}], 
   "\[IndentingNewLine]", 
   RowBox[{"Fc", "=", 
    FractionBox[
     SuperscriptBox[
      RowBox[{"D", "[", 
       RowBox[{
        RowBox[{"probGeqk", "/.", 
         RowBox[{
          SuperscriptBox["\[Xi]\[Sigma]", "2"], "\[Rule]", 
          RowBox[{"\[Xi]", " ", 
           SuperscriptBox["\[Sigma]", "2"]}]}]}], ",", "\[Sigma]"}], "]"}], 
      "2"], 
     RowBox[{"probGeqk", "/.", 
      RowBox[{
       SuperscriptBox["\[Xi]\[Sigma]", "2"], "\[Rule]", 
       RowBox[{"\[Xi]", " ", 
        SuperscriptBox["\[Sigma]", "2"]}]}]}]]}], "\[IndentingNewLine]", 
   RowBox[{"Limit", "[", 
    RowBox[{"Fc", ",", 
     RowBox[{"k", "\[Rule]", "\[Infinity]"}], ",", 
     RowBox[{"Assumptions", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"\[Xi]", ">", "0"}], ",", 
        RowBox[{"\[Sigma]", ">", "0"}], ",", 
        RowBox[{"nbar", ">", "0"}]}], "}"}]}]}], "]"}]}]}]], "Input",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.},
 CellChangeTimes->{{3.921240518885048*^9, 3.92124056830375*^9}, {
   3.921240632056252*^9, 3.921240649324791*^9}, {3.921240700530877*^9, 
   3.921240762448591*^9}, 3.921240823258504*^9, {3.921240943895995*^9, 
   3.92124100304917*^9}, {3.921241199999921*^9, 3.921241295586906*^9}},
 CellLabel->"In[80]:=",ExpressionUUID->"0f070e31-9f15-4a40-87cc-228330cd5b1e"],

Cell[BoxData[
 RowBox[{
  SuperscriptBox[
   RowBox[{"(", 
    FractionBox["nbar", 
     RowBox[{"1", "+", "nbar"}]], ")"}], "k"], "+", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{"1", "-", 
     SuperscriptBox[
      RowBox[{"(", 
       FractionBox["nbar", 
        RowBox[{"1", "+", "nbar"}]], ")"}], "k"]}], ")"}], " ", 
   SuperscriptBox["\[Xi]\[Sigma]", "2"]}]}]], "Output",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.},
 CellChangeTimes->{{3.9212405379509974`*^9, 3.921240558070916*^9}, {
   3.9212406433348217`*^9, 3.921240649715054*^9}, {3.921240712948361*^9, 
   3.921240716725651*^9}, {3.9212408148902817`*^9, 3.921240825797202*^9}, 
   3.921240899739571*^9, {3.9212409463933363`*^9, 3.9212410036496363`*^9}, {
   3.921241204975246*^9, 3.921241295587637*^9}},
 CellLabel->"Out[80]=",ExpressionUUID->"16c55dcf-7b37-4f1b-9268-df16f5a5453d"],

Cell[BoxData["True"], "Output",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.},
 CellChangeTimes->{{3.9212405379509974`*^9, 3.921240558070916*^9}, {
   3.9212406433348217`*^9, 3.921240649715054*^9}, {3.921240712948361*^9, 
   3.921240716725651*^9}, {3.9212408148902817`*^9, 3.921240825797202*^9}, 
   3.921240899739571*^9, {3.9212409463933363`*^9, 3.9212410036496363`*^9}, {
   3.921241204975246*^9, 3.921241295588347*^9}},
 CellLabel->"Out[81]=",ExpressionUUID->"66effd98-9fc8-49c2-aff8-1b4b0a579fb6"],

Cell[BoxData[
 FractionBox[
  RowBox[{"nbar", "+", 
   SuperscriptBox["\[Xi]\[Sigma]", "2"]}], 
  RowBox[{"1", "+", "nbar"}]]], "Output",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.},
 CellChangeTimes->{{3.9212405379509974`*^9, 3.921240558070916*^9}, {
   3.9212406433348217`*^9, 3.921240649715054*^9}, {3.921240712948361*^9, 
   3.921240716725651*^9}, {3.9212408148902817`*^9, 3.921240825797202*^9}, 
   3.921240899739571*^9, {3.9212409463933363`*^9, 3.9212410036496363`*^9}, {
   3.921241204975246*^9, 3.92124129558897*^9}},
 CellLabel->"Out[82]=",ExpressionUUID->"28fb3f19-d60a-4a4e-94c1-e8691389b743"],

Cell[BoxData[
 FractionBox[
  RowBox[{"4", " ", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{"1", "-", 
      SuperscriptBox[
       RowBox[{"(", 
        FractionBox["nbar", 
         RowBox[{"1", "+", "nbar"}]], ")"}], "k"]}], ")"}], "2"], " ", 
   SuperscriptBox["\[Xi]", "2"], " ", 
   SuperscriptBox["\[Sigma]", "2"]}], 
  RowBox[{
   SuperscriptBox[
    RowBox[{"(", 
     FractionBox["nbar", 
      RowBox[{"1", "+", "nbar"}]], ")"}], "k"], "+", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{"1", "-", 
      SuperscriptBox[
       RowBox[{"(", 
        FractionBox["nbar", 
         RowBox[{"1", "+", "nbar"}]], ")"}], "k"]}], ")"}], " ", "\[Xi]", " ", 
    SuperscriptBox["\[Sigma]", "2"]}]}]]], "Output",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.},
 CellChangeTimes->{{3.9212405379509974`*^9, 3.921240558070916*^9}, {
   3.9212406433348217`*^9, 3.921240649715054*^9}, {3.921240712948361*^9, 
   3.921240716725651*^9}, {3.9212408148902817`*^9, 3.921240825797202*^9}, 
   3.921240899739571*^9, {3.9212409463933363`*^9, 3.9212410036496363`*^9}, {
   3.921241204975246*^9, 3.921241295589576*^9}},
 CellLabel->"Out[83]=",ExpressionUUID->"de2bca40-1656-48c6-8109-6619b055da75"],

Cell[BoxData[
 RowBox[{"4", " ", "\[Xi]"}]], "Output",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.},
 CellChangeTimes->{{3.9212405379509974`*^9, 3.921240558070916*^9}, {
   3.9212406433348217`*^9, 3.921240649715054*^9}, {3.921240712948361*^9, 
   3.921240716725651*^9}, {3.9212408148902817`*^9, 3.921240825797202*^9}, 
   3.921240899739571*^9, {3.9212409463933363`*^9, 3.9212410036496363`*^9}, {
   3.921241204975246*^9, 3.921241295590454*^9}},
 CellLabel->"Out[84]=",ExpressionUUID->"02b522cb-50ac-46c7-9722-8ec3808aaa61"]
}, Open  ]]
},
WindowSize->{927, 1025},
WindowMargins->{{0, Automatic}, {0, Automatic}},
Magnification:>1.5 Inherited,
FrontEndVersion->"11.3 for Linux x86 (64-bit) (March 6, 2018)",
StyleDefinitions->"ReverseColor.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 310, 5, 48, "Text",ExpressionUUID->"9b205085-fa49-4540-8e06-eb6dde7a37d1",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.}],
Cell[893, 29, 1090, 30, 161, "Input",ExpressionUUID->"f8cbe6ce-b72f-4cf0-846a-48d0cc06d29b",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.}],
Cell[1986, 61, 322, 5, 53, "Output",ExpressionUUID->"a7dd3247-6c97-4cfd-ba0d-5cd8a30715dd",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.}],
Cell[2311, 68, 504, 11, 79, "Input",ExpressionUUID->"eb3f8e4a-217a-42b5-b107-635e80c80336",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.}],
Cell[2818, 81, 590, 17, 57, "Output",ExpressionUUID->"74a35ebe-cab0-4946-a950-96ee5a536584",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.}],
Cell[3411, 100, 777, 19, 116, "Input",ExpressionUUID->"eb1142d9-c460-4c9f-84f0-5316e55f4621",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.}],
Cell[4191, 121, 667, 17, 57, "Output",ExpressionUUID->"057db5c2-7db8-4dee-b329-f35e08673859",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.}]
}, Open  ]],
Cell[CellGroupData[{
Cell[4895, 143, 499, 14, 47, "Input",ExpressionUUID->"9fe0232d-1e81-4246-8733-5709c1fb34b4"],
Cell[5397, 159, 1542, 49, 88, "Output",ExpressionUUID->"31dc8bce-62b8-4bc6-9910-278ba9991c77"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6976, 213, 1900, 52, 167, "Input",ExpressionUUID->"14470b68-1df5-4f20-8af3-57b851df5cb1"],
Cell[8879, 267, 593, 17, 91, "Output",ExpressionUUID->"97195331-6fc6-4958-9291-d438733732fe"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9509, 289, 644, 17, 116, "Input",ExpressionUUID->"a2b95ef0-fdaf-4138-a820-3b2c95b4ddbe",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.}],
Cell[10156, 308, 842, 25, 93, "Output",ExpressionUUID->"45dfc52c-1d34-4c7a-945b-308ff728574b",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.}],
Cell[11001, 335, 818, 24, 57, "Output",ExpressionUUID->"b3fd5456-61ac-4ce8-aeaa-e70c55e8a07a",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.}],
Cell[11822, 361, 2862, 80, 322, "Input",ExpressionUUID->"0f070e31-9f15-4a40-87cc-228330cd5b1e",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.}],
Cell[14687, 443, 855, 20, 76, "Output",ExpressionUUID->"16c55dcf-7b37-4f1b-9268-df16f5a5453d",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.}],
Cell[15545, 465, 509, 7, 53, "Output",ExpressionUUID->"66effd98-9fc8-49c2-aff8-1b4b0a579fb6",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.}],
Cell[16057, 474, 614, 11, 79, "Output",ExpressionUUID->"28fb3f19-d60a-4a4e-94c1-e8691389b743",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.}],
Cell[16674, 487, 1196, 31, 114, "Output",ExpressionUUID->"de2bca40-1656-48c6-8109-6619b055da75",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.}],
Cell[17873, 520, 532, 8, 53, "Output",ExpressionUUID->"02b522cb-50ac-46c7-9722-8ec3808aaa61",
 CellGroupingRules->{"GroupTogetherGrouping", 10001.}]
}, Open  ]]
}
]
*)

