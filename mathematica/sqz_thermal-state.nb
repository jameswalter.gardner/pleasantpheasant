(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 14.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     32997,        822]
NotebookOptionsPosition[     30839,        780]
NotebookOutlinePosition[     31269,        797]
CellTagsIndexPosition[     31226,        794]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"squeezed", " ", "thermal", " ", "state"}], ",", " ", 
    RowBox[{
     RowBox[{"e", ".", "g", ".", " ", "Eq"}], ".12", " ", "in", " ", "arXiv", 
     " ", "release"}]}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"p", "[", "n_", "]"}], ":=", 
    RowBox[{
     FractionBox[
      RowBox[{
       RowBox[{"(", 
        RowBox[{"2", "n"}], ")"}], "!"}], 
      RowBox[{
       SuperscriptBox["2", 
        RowBox[{"2", "n"}]], 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"n", "!"}], ")"}], "2"]}]], 
     FractionBox[
      SuperscriptBox["\[Sigma]", 
       RowBox[{"2", "n"}]], 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{
         SuperscriptBox["\[Sigma]", "2"], "+", "1"}], ")"}], 
       RowBox[{"n", "+", 
        FractionBox["1", "2"]}]]]}]}], "\[IndentingNewLine]", 
   RowBox[{"(*", 
    RowBox[{"symmetric", " ", "thermal", " ", "state"}], "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"pTherm", "[", "n_", "]"}], ":=", 
    FractionBox[
     SuperscriptBox["nbar", "n"], 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{"nbar", "+", "1"}], ")"}], 
      RowBox[{"n", "+", "1"}]]]}]}]}]], "Input",
 CellChangeTimes->{{3.939482040652987*^9, 3.939482070761904*^9}, {
  3.939482225037965*^9, 3.939482245860525*^9}, {3.939482448582058*^9, 
  3.939482472145327*^9}},
 CellLabel->"In[1]:=",ExpressionUUID->"e7d7a54e-5690-454c-8408-682cde5357bf"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"$Assumptions", "=", 
   RowBox[{"{", 
    RowBox[{"\[Sigma]", ">=", "0"}], "}"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"norm", "=", 
  RowBox[{
   RowBox[{"Sum", "[", 
    RowBox[{
     RowBox[{"p", "[", "n", "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"n", ",", "0", ",", "\[Infinity]"}], "}"}]}], "]"}], "//", 
   "FullSimplify"}]}], "\[IndentingNewLine]", 
 RowBox[{"EV", "=", 
  RowBox[{
   RowBox[{"Sum", "[", 
    RowBox[{
     RowBox[{"n", " ", 
      RowBox[{"p", "[", "n", "]"}]}], ",", 
     RowBox[{"{", 
      RowBox[{"n", ",", "0", ",", "\[Infinity]"}], "}"}]}], "]"}], "//", 
   "FullSimplify"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"EVsqr", "=", 
   RowBox[{
    RowBox[{"Sum", "[", 
     RowBox[{
      RowBox[{
       SuperscriptBox["n", "2"], " ", 
       RowBox[{"p", "[", "n", "]"}]}], ",", 
      RowBox[{"{", 
       RowBox[{"n", ",", "0", ",", "\[Infinity]"}], "}"}]}], "]"}], "//", 
    "FullSimplify"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"Var", "=", 
  RowBox[{
   RowBox[{
    RowBox[{"EVsqr", "-", 
     SuperscriptBox["EV", "2"]}], "//", "FullSimplify"}], "//", 
   "Factor"}]}]}], "Input",
 CellChangeTimes->{{3.939482126192409*^9, 3.939482200199482*^9}},
 CellLabel->"In[3]:=",ExpressionUUID->"4b9ed480-b70f-4632-911b-482825326a0e"],

Cell[BoxData["1"], "Output",
 CellChangeTimes->{{3.939482133560183*^9, 3.9394822008869658`*^9}, 
   3.939482477196733*^9, 3.9394825687121887`*^9, 3.941126049507251*^9},
 CellLabel->"Out[4]=",ExpressionUUID->"46f02ca8-23f6-4cdd-906d-d1712c6a2114"],

Cell[BoxData[
 FractionBox[
  SuperscriptBox["\[Sigma]", "2"], "2"]], "Output",
 CellChangeTimes->{{3.939482133560183*^9, 3.9394822008869658`*^9}, 
   3.939482477196733*^9, 3.9394825687121887`*^9, 3.94112604960175*^9},
 CellLabel->"Out[5]=",ExpressionUUID->"6288fb56-3325-4f02-916a-9aa498ba71b0"],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "2"], " ", 
  SuperscriptBox["\[Sigma]", "2"], " ", 
  RowBox[{"(", 
   RowBox[{"1", "+", 
    SuperscriptBox["\[Sigma]", "2"]}], ")"}]}]], "Output",
 CellChangeTimes->{{3.939482133560183*^9, 3.9394822008869658`*^9}, 
   3.939482477196733*^9, 3.9394825687121887`*^9, 3.9411260497197323`*^9},
 CellLabel->"Out[7]=",ExpressionUUID->"b70db744-9d2c-4961-8b55-64c6eb55327a"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"p", "[", "n", "]"}], "/.", 
   RowBox[{"\[Sigma]", "->", 
    RowBox[{"Sqrt", "[", 
     RowBox[{"2", "nbar"}], "]"}]}]}], "//", "FullSimplify"}]], "Input",
 CellChangeTimes->{{3.941126220753026*^9, 3.941126248718337*^9}},
 CellLabel->"In[28]:=",ExpressionUUID->"e940971d-940d-41a5-a934-5f9e77d6c757"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{"4", "+", 
      FractionBox["2", "nbar"]}], ")"}], 
    RowBox[{"-", "n"}]], " ", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{"2", " ", "n"}], ")"}], "!"}]}], 
  RowBox[{
   SqrtBox[
    RowBox[{"1", "+", 
     RowBox[{"2", " ", "nbar"}]}]], " ", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{"n", "!"}], ")"}], "2"]}]]], "Output",
 CellChangeTimes->{{3.94112624508117*^9, 3.941126248953148*^9}},
 CellLabel->"Out[28]=",ExpressionUUID->"2cf9fcef-033f-4280-abe1-097ae352cd21"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"$Assumptions", "=", 
   RowBox[{"{", 
    RowBox[{"nbar", ">=", "0"}], "}"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"norm", "=", 
  RowBox[{
   RowBox[{"Sum", "[", 
    RowBox[{
     RowBox[{"pTherm", "[", "n", "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"n", ",", "0", ",", "\[Infinity]"}], "}"}]}], "]"}], "//", 
   "FullSimplify"}]}], "\[IndentingNewLine]", 
 RowBox[{"EV", "=", 
  RowBox[{
   RowBox[{"Sum", "[", 
    RowBox[{
     RowBox[{"n", " ", 
      RowBox[{"pTherm", "[", "n", "]"}]}], ",", 
     RowBox[{"{", 
      RowBox[{"n", ",", "0", ",", "\[Infinity]"}], "}"}]}], "]"}], "//", 
   "FullSimplify"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"%", "/.", 
   RowBox[{"nbar", "->", 
    RowBox[{
     SuperscriptBox["\[Sigma]", "2"], "/", "2"}]}]}], "//", 
  "FullSimplify"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"EVsqr", "=", 
   RowBox[{
    RowBox[{"Sum", "[", 
     RowBox[{
      RowBox[{
       SuperscriptBox["n", "2"], " ", 
       RowBox[{"pTherm", "[", "n", "]"}]}], ",", 
      RowBox[{"{", 
       RowBox[{"n", ",", "0", ",", "\[Infinity]"}], "}"}]}], "]"}], "//", 
    "FullSimplify"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"Var", "=", 
  RowBox[{
   RowBox[{
    RowBox[{"EVsqr", "-", 
     SuperscriptBox["EV", "2"]}], "//", "FullSimplify"}], "//", 
   "Factor"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"%", "/.", 
   RowBox[{"nbar", "->", 
    RowBox[{
     SuperscriptBox["\[Sigma]", "2"], "/", "2"}]}]}], "//", 
  "FullSimplify"}]}], "Input",
 CellChangeTimes->{{3.939482314791394*^9, 3.9394823456234403`*^9}},
 CellLabel->"In[20]:=",ExpressionUUID->"9c34f689-f071-4534-b799-05c71b50d787"],

Cell[BoxData["1"], "Output",
 CellChangeTimes->{{3.939482319956685*^9, 3.939482346181849*^9}, 
   3.939482477383456*^9, 3.939482567093763*^9, 3.941126049914734*^9, 
   3.941126178876089*^9},
 CellLabel->"Out[21]=",ExpressionUUID->"1b7b6c39-b3a3-4c03-8457-5650bc4234d4"],

Cell[BoxData["nbar"], "Output",
 CellChangeTimes->{{3.939482319956685*^9, 3.939482346181849*^9}, 
   3.939482477383456*^9, 3.939482567093763*^9, 3.941126049914734*^9, 
   3.941126178951885*^9},
 CellLabel->"Out[22]=",ExpressionUUID->"7b12d57a-d600-4ae3-bfc5-18373b79c75d"],

Cell[BoxData[
 FractionBox[
  SuperscriptBox["\[Sigma]", "2"], "2"]], "Output",
 CellChangeTimes->{{3.939482319956685*^9, 3.939482346181849*^9}, 
   3.939482477383456*^9, 3.939482567093763*^9, 3.941126049914734*^9, 
   3.941126178959819*^9},
 CellLabel->"Out[23]=",ExpressionUUID->"f518b710-6194-44bf-ada1-f6c9da8ff2d4"],

Cell[BoxData[
 RowBox[{"nbar", " ", 
  RowBox[{"(", 
   RowBox[{"1", "+", "nbar"}], ")"}]}]], "Output",
 CellChangeTimes->{{3.939482319956685*^9, 3.939482346181849*^9}, 
   3.939482477383456*^9, 3.939482567093763*^9, 3.941126049914734*^9, 
   3.941126179025838*^9},
 CellLabel->"Out[25]=",ExpressionUUID->"0c995d4b-03f7-409c-a305-95a466e2f813"],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "4"], " ", 
  SuperscriptBox["\[Sigma]", "2"], " ", 
  RowBox[{"(", 
   RowBox[{"2", "+", 
    SuperscriptBox["\[Sigma]", "2"]}], ")"}]}]], "Output",
 CellChangeTimes->{{3.939482319956685*^9, 3.939482346181849*^9}, 
   3.939482477383456*^9, 3.939482567093763*^9, 3.941126049914734*^9, 
   3.9411261790315866`*^9},
 CellLabel->"Out[26]=",ExpressionUUID->"10e075a3-43d4-430f-af7c-2933d7caf9ee"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
   "variance", " ", "of", " ", "squeezed", " ", "thermal", " ", "state", " ", 
    "is", " ", "higher", " ", "than", " ", "symmetric", " ", "thermal", " ", 
    "state", " ", "with", " ", "the", " ", "same", " ", "mean", " ", 
    "number"}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{
     FractionBox["1", "2"], " ", 
     SuperscriptBox["\[Sigma]", "2"], " ", 
     RowBox[{"(", 
      RowBox[{"1", "+", 
       SuperscriptBox["\[Sigma]", "2"]}], ")"}]}], ">", 
    RowBox[{
     FractionBox["1", "4"], " ", 
     SuperscriptBox["\[Sigma]", "2"], " ", 
     RowBox[{"(", 
      RowBox[{"2", "+", 
       SuperscriptBox["\[Sigma]", "2"]}], ")"}]}]}], "//", 
   "FullSimplify"}]}]], "Input",
 CellChangeTimes->{{3.939482547741774*^9, 3.93948256262151*^9}, {
  3.939482599235969*^9, 3.939482615686027*^9}},
 CellLabel->"In[15]:=",ExpressionUUID->"f1b42dbe-6c55-4a5a-90e1-718d0ab4cc76"],

Cell[BoxData[
 RowBox[{
  SuperscriptBox["\[Sigma]", "2"], ">", "0"}]], "Output",
 CellChangeTimes->{{3.939482550217749*^9, 3.939482571491316*^9}, 
   3.941126050572649*^9},
 CellLabel->"Out[15]=",ExpressionUUID->"5e4acafe-f17b-4d71-8174-dc213c661a96"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"\[Sigma]0", "=", "1.5"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"Print", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{
      FractionBox["1", "2"], " ", 
      SuperscriptBox["\[Sigma]", "2"], " ", 
      RowBox[{"(", 
       RowBox[{"1", "+", 
        SuperscriptBox["\[Sigma]", "2"]}], ")"}]}], ",", 
     RowBox[{
      FractionBox["1", "4"], " ", 
      SuperscriptBox["\[Sigma]", "2"], " ", 
      RowBox[{"(", 
       RowBox[{"2", "+", 
        SuperscriptBox["\[Sigma]", "2"]}], ")"}]}]}], "}"}], "/.", 
   RowBox[{"\[Sigma]", "->", "\[Sigma]0"}]}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"ListPlot", "[", 
  RowBox[{
   RowBox[{"{", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"Table", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"p", "[", "n", "]"}], "/.", 
        RowBox[{"\[Sigma]", "->", "\[Sigma]0"}]}], ",", 
       RowBox[{"{", 
        RowBox[{"n", ",", "0", ",", "10"}], "}"}]}], "]"}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{"Table", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"pTherm", "[", "n", "]"}], "/.", 
        RowBox[{"nbar", "->", 
         FractionBox[
          SuperscriptBox["\[Sigma]0", "2"], "2"]}]}], ",", 
       RowBox[{"{", 
        RowBox[{"n", ",", "0", ",", "10"}], "}"}]}], "]"}]}], 
    "\[IndentingNewLine]", "}"}], ",", 
   RowBox[{"PlotRange", "->", "All"}], ",", 
   RowBox[{"PlotLegends", "->", 
    RowBox[{"{", 
     RowBox[{
     "\"\<squeezed thermal state\>\"", ",", " ", 
      "\"\<symmetric thermal state\>\""}], "}"}]}]}], "]"}]}], "Input",
 CellChangeTimes->{{3.9394820767467127`*^9, 3.939482120692763*^9}, {
  3.9394822154077873`*^9, 3.939482223414079*^9}, {3.939482254417321*^9, 
  3.939482296413889*^9}, {3.9394824146132507`*^9, 3.9394824301001863`*^9}, {
  3.939482628998091*^9, 3.939482637917882*^9}, {3.939482673574041*^9, 
  3.939482676326969*^9}},
 CellLabel->"In[16]:=",ExpressionUUID->"1b853441-fdd4-4192-a442-fc002e1d9601"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"3.65625`", ",", "2.390625`"}], "}"}]], "Print",
 CellChangeTimes->{3.939482676795635*^9, 3.941126050607422*^9},
 CellLabel->
  "During evaluation of \
In[16]:=",ExpressionUUID->"1ead7459-963e-4c6c-a060-4699e96f5969"],

Cell[BoxData[
 TemplateBox[{
   GraphicsBox[{{}, 
     InterpretationBox[{
       TagBox[{{
          TagBox[{
            Directive[
             PointSize[0.012833333333333334`], 
             RGBColor[0.368417, 0.506779, 0.709798], 
             AbsoluteThickness[2]], 
            
            PointBox[{{1., 0.5547001962252291}, {2., 0.19201160638565623`}, {
             3., 0.09969833408485997}, {4., 0.0575182696643423}, {5., 
             0.03484279796974581}, {6., 0.021709743350380083`}, {7., 
             0.013777337126202743`}, {8., 0.008856859581130336}, {9., 
             0.0057484425165990165`}, {10., 0.003758597030083972}, {11., 
             0.0024720003544013817`}}]}, 
           Annotation[#, "Charting`Private`Tag#1"]& ], 
          TagBox[{
            Directive[
             PointSize[0.012833333333333334`], 
             RGBColor[0.880722, 0.611041, 0.142051], 
             AbsoluteThickness[2]], 
            
            PointBox[{{1., 0.47058823529411764`}, {2., 
             0.24913494809688583`}, {3., 0.13189497252188073`}, {4., 
             0.06982675015864273}, {5., 0.0369671030251638}, {6., 
             0.01957081924861613}, {7., 0.010361021955149716`}, {8., 
             0.005485246917432203}, {9., 0.002903954250405284}, {10., 
             0.001537387544332209}, {11., 0.0008139110528817577}}]}, 
           Annotation[#, "Charting`Private`Tag#2"]& ]}}, {
        "WolframDynamicHighlight", <|
         "Label" -> {"XYLabel"}, "Ball" -> {"IndicatedBall"}|>}], 
       StyleBox[
        DynamicBox[
         (Charting`HighlightActionBox["DynamicHighlight", {}, 
          Slot["HighlightElements"], 
          Slot["LayoutOptions"], 
          Slot["Meta"], 
          Charting`HighlightActionFunction["DynamicHighlight", {{
             Annotation[{
               Directive[
                PointSize[0.012833333333333334`], 
                RGBColor[0.368417, 0.506779, 0.709798], 
                AbsoluteThickness[2]], 
               
               Point[{{1., 0.5547001962252291}, {2., 0.19201160638565623`}, {
                3., 0.09969833408485997}, {4., 0.0575182696643423}, {5., 
                0.03484279796974581}, {6., 0.021709743350380083`}, {7., 
                0.013777337126202743`}, {8., 0.008856859581130336}, {9., 
                0.0057484425165990165`}, {10., 0.003758597030083972}, {11., 
                0.0024720003544013817`}}]}, "Charting`Private`Tag#1"], 
             Annotation[{
               Directive[
                PointSize[0.012833333333333334`], 
                RGBColor[0.880722, 0.611041, 0.142051], 
                AbsoluteThickness[2]], 
               
               Point[{{1., 0.47058823529411764`}, {2., 
                0.24913494809688583`}, {3., 0.13189497252188073`}, {4., 
                0.06982675015864273}, {5., 0.0369671030251638}, {6., 
                0.01957081924861613}, {7., 0.010361021955149716`}, {8., 
                0.005485246917432203}, {9., 0.002903954250405284}, {10., 
                0.001537387544332209}, {11., 0.0008139110528817577}}]}, 
              "Charting`Private`Tag#2"]}}, <|
           "HighlightElements" -> <|
             "Label" -> {"XYLabel"}, "Ball" -> {"IndicatedBall"}|>, 
            "LayoutOptions" -> <|
             "PanelPlotLayout" -> <||>, 
              "PlotRange" -> {{0.7916666666666685, 11.}, {
                0, 0.5547001962252291}}, 
              "Frame" -> {{False, False}, {False, False}}, 
              "AxesOrigin" -> {0.7916666666666685, 0}, 
              "ImageSize" -> {360, 360/GoldenRatio}, "Axes" -> {True, True}, 
              "LabelStyle" -> {}, "AspectRatio" -> GoldenRatio^(-1), 
              "DefaultStyle" -> {
                Directive[
                 PointSize[0.012833333333333334`], 
                 RGBColor[0.880722, 0.611041, 0.142051], 
                 AbsoluteThickness[2]], 
                Directive[
                 PointSize[0.012833333333333334`], 
                 RGBColor[0.368417, 0.506779, 0.709798], 
                 AbsoluteThickness[2]]}, 
              "HighlightLabelingFunctions" -> <|
               "CoordinatesToolOptions" -> ({
                  Identity[
                   Part[#, 1]], 
                  Identity[
                   Part[#, 2]]}& ), 
                "ScalingFunctions" -> {{Identity, Identity}, {
                  Identity, Identity}}|>, "Primitives" -> {}, "GCFlag" -> 
              False|>, 
            "Meta" -> <|
             "DefaultHighlight" -> {"Dynamic", None}, "Index" -> {}, 
              "Function" -> ListPlot, "GroupHighlight" -> False|>|>]]& )[<|
          "HighlightElements" -> <|
            "Label" -> {"XYLabel"}, "Ball" -> {"IndicatedBall"}|>, 
           "LayoutOptions" -> <|
            "PanelPlotLayout" -> <||>, 
             "PlotRange" -> {{0.7916666666666685, 11.}, {
               0, 0.5547001962252291}}, 
             "Frame" -> {{False, False}, {False, False}}, 
             "AxesOrigin" -> {0.7916666666666685, 0}, 
             "ImageSize" -> {360, 360/GoldenRatio}, "Axes" -> {True, True}, 
             "LabelStyle" -> {}, "AspectRatio" -> GoldenRatio^(-1), 
             "DefaultStyle" -> {
               Directive[
                PointSize[0.012833333333333334`], 
                RGBColor[0.880722, 0.611041, 0.142051], 
                AbsoluteThickness[2]], 
               Directive[
                PointSize[0.012833333333333334`], 
                RGBColor[0.368417, 0.506779, 0.709798], 
                AbsoluteThickness[2]]}, 
             "HighlightLabelingFunctions" -> <|"CoordinatesToolOptions" -> ({
                 Identity[
                  Part[#, 1]], 
                 Identity[
                  Part[#, 2]]}& ), 
               "ScalingFunctions" -> {{Identity, Identity}, {
                 Identity, Identity}}|>, "Primitives" -> {}, "GCFlag" -> 
             False|>, 
           "Meta" -> <|
            "DefaultHighlight" -> {"Dynamic", None}, "Index" -> {}, 
             "Function" -> ListPlot, "GroupHighlight" -> False|>|>]], 
        Selectable -> False]}, 
      Annotation[{{
         Annotation[{
           Directive[
            PointSize[0.012833333333333334`], 
            RGBColor[0.368417, 0.506779, 0.709798], 
            AbsoluteThickness[2]], 
           
           Point[{{1., 0.5547001962252291}, {2., 0.19201160638565623`}, {3., 
            0.09969833408485997}, {4., 0.0575182696643423}, {5., 
            0.03484279796974581}, {6., 0.021709743350380083`}, {7., 
            0.013777337126202743`}, {8., 0.008856859581130336}, {9., 
            0.0057484425165990165`}, {10., 0.003758597030083972}, {11., 
            0.0024720003544013817`}}]}, "Charting`Private`Tag#1"], 
         Annotation[{
           Directive[
            PointSize[0.012833333333333334`], 
            RGBColor[0.880722, 0.611041, 0.142051], 
            AbsoluteThickness[2]], 
           
           Point[{{1., 0.47058823529411764`}, {2., 0.24913494809688583`}, {3.,
             0.13189497252188073`}, {4., 0.06982675015864273}, {5., 
            0.0369671030251638}, {6., 0.01957081924861613}, {7., 
            0.010361021955149716`}, {8., 0.005485246917432203}, {9., 
            0.002903954250405284}, {10., 0.001537387544332209}, {11., 
            0.0008139110528817577}}]}, "Charting`Private`Tag#2"]}}, <|
       "HighlightElements" -> <|
         "Label" -> {"XYLabel"}, "Ball" -> {"IndicatedBall"}|>, 
        "LayoutOptions" -> <|
         "PanelPlotLayout" -> <||>, 
          "PlotRange" -> {{0.7916666666666685, 11.}, {0, 0.5547001962252291}},
           "Frame" -> {{False, False}, {False, False}}, 
          "AxesOrigin" -> {0.7916666666666685, 0}, 
          "ImageSize" -> {360, 360/GoldenRatio}, "Axes" -> {True, True}, 
          "LabelStyle" -> {}, "AspectRatio" -> GoldenRatio^(-1), 
          "DefaultStyle" -> {
            Directive[
             PointSize[0.012833333333333334`], 
             RGBColor[0.880722, 0.611041, 0.142051], 
             AbsoluteThickness[2]], 
            Directive[
             PointSize[0.012833333333333334`], 
             RGBColor[0.368417, 0.506779, 0.709798], 
             AbsoluteThickness[2]]}, 
          "HighlightLabelingFunctions" -> <|"CoordinatesToolOptions" -> ({
              Identity[
               Part[#, 1]], 
              Identity[
               Part[#, 2]]}& ), 
            "ScalingFunctions" -> {{Identity, Identity}, {
              Identity, Identity}}|>, "Primitives" -> {}, "GCFlag" -> False|>,
         "Meta" -> <|
         "DefaultHighlight" -> {"Dynamic", None}, "Index" -> {}, "Function" -> 
          ListPlot, "GroupHighlight" -> False|>|>, 
       "DynamicHighlight"]], {{}, {}}}, {
    DisplayFunction -> Identity, DisplayFunction -> Identity, DisplayFunction -> 
     Identity, AspectRatio -> NCache[GoldenRatio^(-1), 0.6180339887498948], 
     Axes -> {True, True}, AxesLabel -> {None, None}, 
     AxesOrigin -> {0.7916666666666685, 0}, DisplayFunction :> Identity, 
     Frame -> {{False, False}, {False, False}}, 
     FrameLabel -> {{None, None}, {None, None}}, 
     FrameTicks -> {{Automatic, Automatic}, {Automatic, Automatic}}, 
     GridLines -> {None, None}, GridLinesStyle -> Directive[
       GrayLevel[0.5, 0.4]], 
     Method -> {
      "AxisPadding" -> Scaled[0.02], "DefaultBoundaryStyle" -> Automatic, 
       "DefaultGraphicsInteraction" -> {
        "Version" -> 1.2, "TrackMousePosition" -> {True, False}, 
         "Effects" -> {
          "Highlight" -> {"ratio" -> 2}, "HighlightPoint" -> {"ratio" -> 2}, 
           "Droplines" -> {
            "freeformCursorMode" -> True, 
             "placement" -> {"x" -> "All", "y" -> "None"}}}}, 
       "DefaultMeshStyle" -> AbsolutePointSize[6], "DefaultPlotStyle" -> {
         Directive[
          RGBColor[0.368417, 0.506779, 0.709798], 
          AbsoluteThickness[2]], 
         Directive[
          RGBColor[0.880722, 0.611041, 0.142051], 
          AbsoluteThickness[2]], 
         Directive[
          RGBColor[0.560181, 0.691569, 0.194885], 
          AbsoluteThickness[2]], 
         Directive[
          RGBColor[0.922526, 0.385626, 0.209179], 
          AbsoluteThickness[2]], 
         Directive[
          RGBColor[0.528488, 0.470624, 0.701351], 
          AbsoluteThickness[2]], 
         Directive[
          RGBColor[0.772079, 0.431554, 0.102387], 
          AbsoluteThickness[2]], 
         Directive[
          RGBColor[0.363898, 0.618501, 0.782349], 
          AbsoluteThickness[2]], 
         Directive[
          RGBColor[1, 0.75, 0], 
          AbsoluteThickness[2]], 
         Directive[
          RGBColor[0.647624, 0.37816, 0.614037], 
          AbsoluteThickness[2]], 
         Directive[
          RGBColor[0.571589, 0.586483, 0.], 
          AbsoluteThickness[2]], 
         Directive[
          RGBColor[0.915, 0.3325, 0.2125], 
          AbsoluteThickness[2]], 
         Directive[
          RGBColor[0.40082222609352647`, 0.5220066643438841, 0.85], 
          AbsoluteThickness[2]], 
         Directive[
          RGBColor[
          0.9728288904374106, 0.621644452187053, 0.07336199581899142], 
          AbsoluteThickness[2]], 
         Directive[
          RGBColor[0.736782672705901, 0.358, 0.5030266573755369], 
          AbsoluteThickness[2]], 
         Directive[
          RGBColor[0.28026441037696703`, 0.715, 0.4292089322474965], 
          AbsoluteThickness[2]]}, "DomainPadding" -> Scaled[0.02], 
       "PointSizeFunction" -> "SmallPointSize", "RangePadding" -> 
       Scaled[0.05], "OptimizePlotMarkers" -> True, "IncludeHighlighting" -> 
       "CurrentPoint", "HighlightStyle" -> Automatic, "OptimizePlotMarkers" -> 
       True, "CoordinatesToolOptions" -> {"DisplayFunction" -> ({
           Identity[
            Part[#, 1]], 
           Identity[
            Part[#, 2]]}& ), "CopiedValueFunction" -> ({
           Identity[
            Part[#, 1]], 
           Identity[
            Part[#, 2]]}& )}}, 
     PlotRange -> {{0.7916666666666685, 11.}, {0, 0.5547001962252291}}, 
     PlotRangeClipping -> True, PlotRangePadding -> {{
        Scaled[0.02], 
        Scaled[0.02]}, {
        Scaled[0.02], 
        Scaled[0.05]}}, Ticks -> {Automatic, Automatic}}], 
   FormBox[
    FormBox[
     TemplateBox[{
      "\"squeezed thermal state\"", "\"symmetric thermal state\""}, 
      "PointLegend", DisplayFunction -> (FormBox[
        StyleBox[
         StyleBox[
          PaneBox[
           TagBox[
            GridBox[{{
               TagBox[
                GridBox[{{
                   GraphicsBox[{{}, {
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    RGBColor[0.368417, 0.506779, 0.709798], 
                    AbsoluteThickness[2]], {
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    RGBColor[0.368417, 0.506779, 0.709798], 
                    AbsoluteThickness[2]], 
                    PointBox[
                    NCache[{
                    Scaled[{
                    Rational[1, 2], 
                    Rational[1, 2]}]}, {
                    Scaled[{0.5, 0.5}]}]]}}}, AspectRatio -> Full, 
                    ImageSize -> {10, 12.5}, PlotRangePadding -> None, 
                    ImagePadding -> Automatic, 
                    BaselinePosition -> (Scaled[0.18000000000000002`] -> 
                    Baseline)], #}, {
                   GraphicsBox[{{}, {
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    RGBColor[0.880722, 0.611041, 0.142051], 
                    AbsoluteThickness[2]], {
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    RGBColor[0.880722, 0.611041, 0.142051], 
                    AbsoluteThickness[2]], 
                    PointBox[
                    NCache[{
                    Scaled[{
                    Rational[1, 2], 
                    Rational[1, 2]}]}, {
                    Scaled[{0.5, 0.5}]}]]}}}, AspectRatio -> Full, 
                    ImageSize -> {10, 12.5}, PlotRangePadding -> None, 
                    ImagePadding -> Automatic, 
                    BaselinePosition -> (Scaled[0.18000000000000002`] -> 
                    Baseline)], #2}}, 
                 GridBoxAlignment -> {
                  "Columns" -> {Center, Left}, "Rows" -> {{Baseline}}}, 
                 AutoDelete -> False, 
                 GridBoxDividers -> {
                  "Columns" -> {{False}}, "Rows" -> {{False}}}, 
                 GridBoxItemSize -> {"Columns" -> {{All}}, "Rows" -> {{All}}},
                  GridBoxSpacings -> {
                  "Columns" -> {{0.5}}, "Rows" -> {{0.8}}}], "Grid"]}}, 
             GridBoxAlignment -> {"Columns" -> {{Left}}, "Rows" -> {{Top}}}, 
             AutoDelete -> False, 
             GridBoxItemSize -> {
              "Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}, 
             GridBoxSpacings -> {"Columns" -> {{1}}, "Rows" -> {{0}}}], 
            "Grid"], Alignment -> Left, AppearanceElements -> None, 
           ImageMargins -> {{5, 5}, {5, 5}}, ImageSizeAction -> 
           "ResizeToFit"], LineIndent -> 0, StripOnInput -> False], {
         FontFamily -> "Arial"}, Background -> Automatic, StripOnInput -> 
         False], TraditionalForm]& ), 
      InterpretationFunction :> (RowBox[{"PointLegend", "[", 
         RowBox[{
           RowBox[{"{", 
             RowBox[{
               RowBox[{"Directive", "[", 
                 RowBox[{
                   RowBox[{"PointSize", "[", "0.012833333333333334`", "]"}], 
                   ",", 
                   
                   TemplateBox[<|
                    "color" -> RGBColor[0.368417, 0.506779, 0.709798]|>, 
                    "RGBColorSwatchTemplate"], ",", 
                   RowBox[{"AbsoluteThickness", "[", "2", "]"}]}], "]"}], ",", 
               RowBox[{"Directive", "[", 
                 RowBox[{
                   RowBox[{"PointSize", "[", "0.012833333333333334`", "]"}], 
                   ",", 
                   
                   TemplateBox[<|
                    "color" -> RGBColor[0.880722, 0.611041, 0.142051]|>, 
                    "RGBColorSwatchTemplate"], ",", 
                   RowBox[{"AbsoluteThickness", "[", "2", "]"}]}], "]"}]}], 
             "}"}], ",", 
           RowBox[{"{", 
             RowBox[{#, ",", #2}], "}"}], ",", 
           RowBox[{"LegendMarkers", "\[Rule]", 
             RowBox[{"{", 
               RowBox[{
                 RowBox[{"{", 
                   RowBox[{"False", ",", "Automatic"}], "}"}], ",", 
                 RowBox[{"{", 
                   RowBox[{"False", ",", "Automatic"}], "}"}]}], "}"}]}], ",", 
           RowBox[{"Joined", "\[Rule]", 
             RowBox[{"{", 
               RowBox[{"False", ",", "False"}], "}"}]}], ",", 
           RowBox[{"LabelStyle", "\[Rule]", 
             RowBox[{"{", "}"}]}], ",", 
           RowBox[{"LegendLayout", "\[Rule]", "\"Column\""}]}], "]"}]& ), 
      Editable -> True], TraditionalForm], TraditionalForm]},
  "Legended",
  DisplayFunction->(GridBox[{{
      TagBox[
       ItemBox[
        PaneBox[
         TagBox[#, "SkipImageSizeLevel"], Alignment -> {Center, Baseline}, 
         BaselinePosition -> Baseline], DefaultBaseStyle -> "Labeled"], 
       "SkipImageSizeLevel"], 
      ItemBox[#2, DefaultBaseStyle -> "LabeledLabel"]}}, 
    GridBoxAlignment -> {"Columns" -> {{Center}}, "Rows" -> {{Center}}}, 
    AutoDelete -> False, GridBoxItemSize -> Automatic, 
    BaselinePosition -> {1, 1}]& ),
  Editable->True,
  InterpretationFunction->(RowBox[{"Legended", "[", 
     RowBox[{#, ",", 
       RowBox[{"Placed", "[", 
         RowBox[{#2, ",", "After"}], "]"}]}], "]"}]& )]], "Output",
 CellChangeTimes->{{3.939482088710647*^9, 3.939482120967911*^9}, {
   3.9394822647915573`*^9, 3.939482297157337*^9}, {3.9394824272339363`*^9, 
   3.939482430619495*^9}, 3.939482478005673*^9, 3.939482638376973*^9, 
   3.939482676896516*^9, 3.941126050987245*^9},
 CellLabel->"Out[18]=",ExpressionUUID->"1ed2662a-48c6-41c4-a2f0-3df055bc6f37"]
}, Open  ]]
},
WindowSize->{1387.5, 852},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
Magnification:>1.5 Inherited,
FrontEndVersion->"14.0 for Linux x86 (64-bit) (December 12, 2023)",
StyleDefinitions->"ReverseColor.nb",
ExpressionUUID->"ff0bb94b-6864-4935-80bb-b1b74049ddff"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 1496, 45, 227, "Input",ExpressionUUID->"e7d7a54e-5690-454c-8408-682cde5357bf"],
Cell[CellGroupData[{
Cell[2079, 69, 1323, 40, 174, "Input",ExpressionUUID->"4b9ed480-b70f-4632-911b-482825326a0e"],
Cell[3405, 111, 246, 3, 50, "Output",ExpressionUUID->"46f02ca8-23f6-4cdd-906d-d1712c6a2114"],
Cell[3654, 116, 296, 5, 70, "Output",ExpressionUUID->"6288fb56-3325-4f02-916a-9aa498ba71b0"],
Cell[3953, 123, 412, 9, 66, "Output",ExpressionUUID->"b70db744-9d2c-4961-8b55-64c6eb55327a"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4402, 137, 348, 8, 47, "Input",ExpressionUUID->"e940971d-940d-41a5-a934-5f9e77d6c757"],
Cell[4753, 147, 570, 19, 93, "Output",ExpressionUUID->"2cf9fcef-033f-4280-abe1-097ae352cd21"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5360, 171, 1689, 52, 240, "Input",ExpressionUUID->"9c34f689-f071-4534-b799-05c71b50d787"],
Cell[7052, 225, 269, 4, 50, "Output",ExpressionUUID->"1b7b6c39-b3a3-4c03-8457-5650bc4234d4"],
Cell[7324, 231, 272, 4, 50, "Output",ExpressionUUID->"7b12d57a-d600-4ae3-bfc5-18373b79c75d"],
Cell[7599, 237, 320, 6, 70, "Output",ExpressionUUID->"f518b710-6194-44bf-ada1-f6c9da8ff2d4"],
Cell[7922, 245, 344, 7, 50, "Output",ExpressionUUID->"0c995d4b-03f7-409c-a305-95a466e2f813"],
Cell[8269, 254, 435, 10, 66, "Output",ExpressionUUID->"10e075a3-43d4-430f-af7c-2933d7caf9ee"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8741, 269, 963, 25, 97, "Input",ExpressionUUID->"f1b42dbe-6c55-4a5a-90e1-718d0ab4cc76"],
Cell[9707, 296, 252, 5, 50, "Output",ExpressionUUID->"5e4acafe-f17b-4d71-8174-dc213c661a96"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9996, 306, 1981, 53, 272, "Input",ExpressionUUID->"1b853441-fdd4-4192-a442-fc002e1d9601"],
Cell[11980, 361, 256, 6, 34, "Print",ExpressionUUID->"1ead7459-963e-4c6c-a060-4699e96f5969"],
Cell[12239, 369, 18584, 408, 366, "Output",ExpressionUUID->"1ed2662a-48c6-41c4-a2f0-3df055bc6f37"]
}, Open  ]]
}
]
*)

