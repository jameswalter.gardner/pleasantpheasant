from operators import *
from states import *
from loss_channels import *
from encoding_channels import *
from fisher_information import *

import os
from scipy.optimize import minimize, basinhopping, NonlinearConstraint


def construct_objective_function(settings):
    """Returns objective function to minimise over to find the channel QFI given settings."""
    construct_total_channel(settings)

    def negative_FI(ket_parts):
        """Returns the negative FI for initial pure state.
        Since to minimise the negative FI is to maximise the FI."""
        if settings["short"]:
            ket_parts = extend(ket_parts)
        # overriding unnormalised parts, TODO: fix the constraints in the optimiser
        # ket_parts = normalise(ket_parts)
        fis = FI_from_state(ket_parts, settings)
        if type(fis) == tuple:
            return [-fi for fi in fis]
        else:
            return -fis

    return negative_FI


def construct_bounds(settings):
    if "short" in settings.keys() and settings["short"]:
        return [(-1, 1) for _ in range(2 * (settings["N"] - 1))]
    else:
        return [(-1, 1) for _ in range(2 * settings["N"])]


def construct_constraints(settings):
    """Returns constraints for optimiser given settings"""
    if settings["method"] == "SLSQP":
        assert not settings["short"]  # TODO: write this
        normalisation_constraint = lambda ket_parts: norm(settings["N"], ket_parts) - 1
        # energy_constraint = (
        #     lambda ket_parts: average_number(ket_parts) - settings["nbar0"]
        # )
        # TODO: fix norm constraint to not normalise here
        energy_ineq = lambda ket_parts: settings["nbar0"] - average_number(
            settings["N"], normalise(settings["N"], ket_parts)
        )
        # 'eq' constraints equal zero, 'ineq' constraints are non-negative
        return (
            {"type": "eq", "fun": normalisation_constraint},
            # {"type": "eq", "fun": energy_constraint},
            {"type": "ineq", "fun": energy_ineq},
        )
    elif settings["method"] == "trust-constr":
        # https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.NonlinearConstraint.html
        # lb <= fun(x) <= ub, keep_feasible forces the optimiser to not explore forbidden regions
        # keep_feasible: Has no effect for equality constraints. TODO: insist on this
        # swapping between equality for long and inequality for short but always calculate norm on whatever's given
        return [
            NonlinearConstraint(
                norm, 1 - int(settings["short"]), 1, keep_feasible=True
            ),
            NonlinearConstraint(
                lambda parts: average_number(
                    settings["N"], parts, short=settings["short"]
                ),
                0,
                settings["nbar0"],
                keep_feasible=True,
            ),
        ]
    else:
        raise ValueError("Method not recognised.")


def construct_callback_minimize(initial_guess=None):
    """
    # From closure for global variable, https://stackoverflow.com/a/27564568
    Generate a callback that prints

        iteration number | parameter values | objective function

    every iteration.
    """
    saved_params = {"iteration_number": 0}

    # generate a unique log filename, https://stackoverflow.com/a/56680778
    filename = "./data/optimisation/callback/log{}.txt"
    i = 0
    while os.path.isfile(filename.format(i)):
        i += 1
    filename = filename.format(i)
    print(f"Callbacks will be saved to {filename}")
    if initial_guess is not None:
        with open(filename, "a") as file:
            file.write(f"Initial guess: {list(initial_guess)}.")

    def callback_minimize(xs, *status):
        # TODO: use status with trust-constr, see https://stackoverflow.com/a/56089633

        # message = f'Iteration: {saved_params["iteration_number"]}, parts: {[f"{v:.2f}" for v in xs]}.'
        # print(message)

        # Save callback without formatting to output file
        with open(filename, "a") as file:
            file.write(f'Iteration: {saved_params["iteration_number"]}, ')
            if initial_guess is not None:
                change = xs - initial_guess
                changed = np.any(change)
                if not changed:
                    file.write("not changed from initial guess.\n")
                else:
                    file.write(
                        f"changed from initial guess by: {list(change)}.\nNew parts: {list(xs)}.\n"
                    )
            else:
                file.write(f"parts: {list(xs)}.\n")

        saved_params["iteration_number"] += 1

    return callback_minimize


def callback_basinhopping(x, f, accepted):
    """Prints the acceptance of the result after each local minimisation.

    Prints for all including the first (despite accept_test not being run there).

    TODO: set up my own tolerance condition using this feature from https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.basinhopping.html
    "Also, callback can be used to specify a user defined stop criterion by optionally returning True to stop the basinhopping routine."

    Args:
        x (array): current args
        f (float): current fn value
        accepted (bool or "force accept"): whether accepted by accept_test + internal temperature check
    """
    # https://stackoverflow.com/a/48771519
    # TODO: closure for global variable: https://stackoverflow.com/a/27564568
    dp = 2
    x_formatted = [f"{v:.{dp}f}" for v in x]
    print(
        f"{'Accepted' if accepted else 'Rejected'} the local minima of {f:.{dp}f} at {x_formatted}."
    )


def optimise(initial_guess_ket_parts, settings, local):
    """Returns result of optimising the FI."""
    # TODO: see if accept_test is still needed like previously to handle the constraints at the first iteration
    # TODO: see if callback useful to print out results at each stage
    objective = construct_objective_function(settings)
    bounds = [(-1, 1) for _ in range(2 * settings["N"])]
    constraints = construct_constraints(settings, trustconstr=False)
    # constraints = construct_constraints(settings, trustconstr=True)

    if local:
        results = minimize(
            objective,
            initial_guess_ket_parts,
            bounds=bounds,
            constraints=constraints,
            # tol=1e-5,
            options=dict(
                disp=True,
                # maxiter=5
            ),
            # SLSQP supports bounds and constraints (eq and ineq)
            method="SLSQP",
            # method="trust-constr"
            callback=construct_callback_minimize(),
            # callback=callback_minimize,
        )
    else:
        # just a single minimize is already slow (did not finish in 2 min) without further outer optimisations
        minimizer_kwargs = {"bounds": bounds, "constraints": constraints}
        results = basinhopping(
            objective,
            initial_guess_ket_parts,
            minimizer_kwargs=minimizer_kwargs,
            niter=5,
            callback=callback_basinhopping,
        )

    return results
