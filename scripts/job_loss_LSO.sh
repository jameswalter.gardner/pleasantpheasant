#!/bin/bash
#
#SBATCH --job-name=lossLSO
#SBATCH --output=slurm_output/lossLSO/stdout_JOB_%A.txt
#SBATCH --error=slurm_output/lossLSO/stderr_JOB_%A.txt
#
#SBATCH --ntasks=1
#SBATCH --time=07-00:00:00 # DD-HH:MM:SS
#SBATCH --mem-per-cpu=100G # MB, TODO: check required MBs

srun python3 -u ./scripts/loss_LSO.py
