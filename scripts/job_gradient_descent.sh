#!/bin/bash
#
#SBATCH --job-name=gradDescent
#SBATCH --output=slurm_output/gradient_descent/stdout_JOB_%A_TASK_%a.txt
#SBATCH --error=slurm_output/gradient_descent/stderr_JOB_%A_TASK_%a.txt
#
#SBATCH --time=07-00:00:00 # DD-HH:MM:SS
#SBATCH --mem-per-cpu=2G
#
#SBATCH --ntasks=1
#SBATCH --array=20,25,30

# argument/s: task_id (handle everything else inside python); -u unbuffers the stdout/stderr without needing flush=True
srun python3 -u ./scripts/gradient_descent.py ${SLURM_ARRAY_TASK_ID}
