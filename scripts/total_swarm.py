#!/usr/bin/env python3
import sys, os

# add directory to import path
sys.path.insert(1, os.path.join(sys.path[0], "../python_source"))
# sys.path.insert(1, "/central/groups/RelativityTheory/jwgardne/pleasantpheasant/python_source")

import pickle
import math
import numpy as np

from states import fock_ket, average_number, normalise, real_to_complex
from loss_channels import construct_M_k_cache
from encoding_channels import construct_total_channel
from fisher_information import QFI_finite_difference
from pso import pso

# -----------------------
# ---- user settings ----
# -----------------------
label = f"{os.environ['SLURM_JOB_ID']}"
# N = 100
# N = 500
N = 800
# N = 1000
nmax = N - 10
kmax = 200
with_phases = True

# specific options to swarm
# number of cores you have allocated for your slurm task:
number_of_cores = int(os.environ["SLURM_CPUS_PER_TASK"])
maxiter = 1000
swarmsize = 100
# print_every = 5
print_every = 2

options = dict(
    nmax=nmax,
    kmax=kmax,
    with_phases=with_phases,
    number_of_cores=number_of_cores,
    maxiter=maxiter,
    swarmsize=swarmsize,
    print_every=print_every,
)
# -----------------------
# -----------------------

settings = dict(
    N=N,
    σ=1e-3,
    δσ=1e-5,
    η=1e-1,
    small_signal=True,
    cached_losses=True,
)

print(
    f"State can access all of |{0}> to |{nmax}> inside the truncated Hilbert space of dimension N = {N}.\nOptimiser set for {maxiter} maximum iterations with a loss tolerance of {kmax} for η = {settings['η']} and with complex phases = {with_phases}."
)
print(f"{number_of_cores} cores are available to the swarm.")

# TODO: see if the memory requirement need to increase for the cache
cache = construct_M_k_cache(N, settings["η"], kmax)
print("Calculated cached loss Kraus operators.")
settings["cache"] = cache
construct_total_channel(settings)

# all of the states, not just those at discrete peaks
kets = np.array([fock_ket(N, n) for n in range(nmax + 1)])


def construct_ket(ps):
    """Return normalised ket from probabilities of all peaks, including \ket0"""
    ps /= np.sum(ps)
    return normalise(N, np.sum(np.sqrt(ps)[:, np.newaxis] * kets, axis=0))


def construct_ket_with_phases(ket_parts_peaks):
    """Return normalised ket from real 2*m vector of ket parts between -1 and 1 for m peaks"""
    # probability amplitudes for the peaks, real_to_complex still works even with the gaps
    ket_peaks = real_to_complex(ket_parts_peaks)
    # renormalise, can't we just use np.abs here?
    norm_peaks = (ket_peaks.conj() @ ket_peaks).real
    ket_peaks /= norm_peaks**0.5
    # construct ket
    return normalise(N, np.sum(ket_peaks[:, np.newaxis] * kets, axis=0))


def callback(it, g, fg):
    if it % print_every == 0:
        # print("Best after iteration {:}: {:} {:}".format(it, g, fg))
        print(f"Best after iteration {it}: QFI = {-fg:.3f}, coeffs = {g}.")

        file = f"./data/total_swarm/{label}.npy"
        ket = construct_ket_with_phases(g) if with_phases else construct_ket(g)
        np.save(file, ket)

        nbar = average_number(settings["N"], ket)
        filename = f"./data/total_swarm/{label}.pkl"
        full_results = dict(
            it=it,
            g=g,
            fg=fg,
            ket=ket,
            nbar=nbar,
            settings={k: v for k, v in settings.items() if k != "channel"},
            options=options,
        )

        with open(filename, "wb") as file:
            pickle.dump(full_results, file)


# def norm_constraint(ps):
#     return 1 - np.sum(ps)

if with_phases:

    def objective(ket_parts_peaks):
        return -QFI_finite_difference(
            construct_ket_with_phases(ket_parts_peaks), settings
        )

    lower_bounds = [-1 for _ in range(2 * (nmax + 1))]
    upper_bounds = [1 for _ in range(2 * (nmax + 1))]
else:

    def objective(ps):
        return -QFI_finite_difference(construct_ket(ps), settings)

    lower_bounds = [0 for _ in range(nmax + 1)]
    upper_bounds = [1 for _ in range(nmax + 1)]

# constraints = [norm_constraint]

# TODO: add checkpoint print outs/callback after every e.g. 100 iterations
ps_opt, obj_opt = pso(
    objective,
    lower_bounds,
    upper_bounds,
    # ieqcons=constraints,
    swarmsize=swarmsize,
    maxiter=maxiter,
    processes=number_of_cores,
    debug=True,
    print_every=print_every,
    callback=callback,
)

nbar = (
    average_number(settings["N"], construct_ket_with_phases(ps_opt))
    if with_phases
    else average_number(settings["N"], construct_ket(ps_opt))
)
print(f"QFI = {-obj_opt:.3f}, nbar = {nbar:.2f}, ps = {ps_opt}.")

callback(maxiter, ps_opt, obj_opt)
