import math
import numpy as np
from scipy.linalg import expm


def dag(X):
    """Hermitian conjugate"""
    return X.conj().T


def Nhat(N):
    """Number operator

    N is the dimension of truncated Hilbert space such that the maximum occupation of the bosonic system is N - 1. This is like in QuTiP.
    """
    return np.diag(np.arange(N))


def a(N):
    """Annihilation operator"""
    return np.diag(np.sqrt(np.arange(1, N)), k=1)


def ad(N):
    """Creation operator"""
    return dag(a(N))


def x_quad(N):
    """x quadrature operator"""
    return (a(N) + ad(N)) / math.sqrt(2)


def p_quad(N):
    """p quadrature operator"""
    return (-1j * a(N) + 1j * ad(N)) / math.sqrt(2)


def exp_matrix(exponent, truncation=None):
    """Returns exp(exponent).

    Args:
        exponent (matrix)
        truncation (None or int): truncation index
    """
    if truncation is None:
        return expm(exponent)
    else:
        summands = (
            1 / np.math.factorial(i) * np.linalg.matrix_power(exponent, i)
            for i in range(truncation + 1)
        )
        return np.sum(summands)


def displacement_operator(N, α, encoding_truncation):
    """Returns the coherent displacement operator \hat D(α).

    Args:
        α (float): coherent amplitude
        encoding_truncation (int): truncation index
    """
    exponent = α * ad(N) - np.conj(α) * a(N)
    return exp_matrix(exponent, truncation=encoding_truncation)


# def displace(N, ρ, α, encoding_truncation=None):
#     D = displacement_operator(N, α, encoding_truncation)
#     return D @ ρ @ dag(D)
