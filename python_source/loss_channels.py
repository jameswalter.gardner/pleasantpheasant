from operators import *
from states import *


def loss_master_eq(N, ρ, η, in_place=False):
    """Return a copy of ρ after a loss η via solving the master equation via euler method.

    "It's crude, but it's quick".
    Needs a and ad defined.

    Args:
        ρ (Nmax-by-Nmax): density matrix
        η (float): lost power fraction
        in_place (bool): whether to instead modify ρ in-place and return None
    """
    # fictional time, a tool for solving the master equation
    tmax = 1
    tsteps = 10000
    dt = tmax / tsteps
    # loss rate
    gamma = η / tmax

    ρ_lossy = ρ.copy()
    for _ in range(tsteps):
        # master equation
        ρ_lossy += (
            gamma
            * dt
            * (
                a(N) @ ρ_lossy @ ad(N)
                - 0.5 * (ad(N) @ a(N) @ ρ_lossy + ρ_lossy @ ad(N) @ a(N))
            )
        )
    # renormalising
    ρ_lossy /= np.trace(ρ_lossy)

    if in_place:
        # since we are modifying the passed ρ with whatever name in its original context, I think that global won't work here
        # global ρ
        ρ = ρ_lossy
    else:
        return ρ_lossy


def loss_binomial(N, ρ, η):
    """Return a copy of ρ after a loss via binomial expansion.

    Here, i and j go from 0 to Nmax whereas in Mathematica they only go over n+-2 (because I know the support of the density matrix for an encoded Fock state)
    TODO: get this to agree with the master equation implementation.

    Args:
        ρ (Nmax-by-Nmax): density matrix
        η (float): lost power fraction
    """
    if η == 0:
        return ρ
    ρ_loss = np.zeros((N, N), dtype=ρ.dtype)
    # TODO: rewrite this without using three nested loops.
    for i in range(N):
        for j in range(N):
            for k in range(0, min(i, j) + 1):
                coeff = (
                    ρ[i, j]
                    * η**k
                    * (1 - η) ** (0.5 * (i + j) - k)
                    * math.sqrt(math.comb(i, k) * math.comb(j, k))
                )
                ρ_loss += coeff * basis_element(N, i - k, j - k)
    return ρ_loss


def M_0(N, η):
    # return np.diag([(1 - η) ** (n / 2) for n in range(N)])
    return np.diag((1 - η) ** (np.arange(N) / 2))


# def factorial_ramanujan(n):
#     # Better than Stirling's approximation, see https://www.johndcook.com/blog/2012/09/25/ramanujans-factorial-approximation/
#     return math.sqrt(math.pi)*(n/math.e)**n * (((8*n + 4)*n + 1)*n + 1/30.)**(1./6.)


# def factorial_invSqrt_ramanujan(n):
#     # https://www.johndcook.com/blog/2012/09/25/ramanujans-factorial-approximation/
#     return (
#         math.pi ** (-1.0 / 4.0)
#         * (n / math.e) ** (-n / 2.0)
#         * (((8 * n + 4) * n + 1) * n + 1 / 30.0) ** (-1.0 / 12.0)
#     )


def K_k(N, η, k):
    """kth loss Kraus operator up to M_0, using Ramanujan's approximation to the factorial for large k."""
    # TODO: look for pre-made calculation to avoid overflow/underflow here for large k, truncate after 15 losses
    # TODO: try caching the K_k results again for N=1000 up to the tolerance and see if this increases speed (will still be dominated by the matrix multiplication)
    # x = factorial_invSqrt_ramanujan(k) if k > 150 else 1 / math.sqrt(math.factorial(k))
    # return x * math.sqrt(η / (1 - η)) ** k * np.linalg.matrix_power(a(N), k)
    if k < 150:
        return (
            1
            / math.sqrt(math.factorial(k))
            * math.sqrt(η / (1 - η)) ** k
            * np.linalg.matrix_power(a(N), k)
        )
    else:
        # grouping all terms to be raised to the kth power to avoid over/underflow
        # including <remaining Ramanujan factors>**(1./k) in X does not change the zeros
        X = math.sqrt(η * math.e / ((1 - η) * k)) * a(N)
        return (
            math.pi ** (-1.0 / 4.0)
            * (((8 * k + 4) * k + 1) * k + 1 / 30.0) ** (-1.0 / 12.0)
            * np.linalg.matrix_power(X, k)
        )


def loss_kraus(N, state, η, truncate=True, tol=1e-6, renormalise=True):
    """Density matrix after pure/cold loss channel via the (truncated) Kraus representation."""
    ρ = ρ_from_state(N, state)
    if η == 0:
        return ρ

    # M_0 is Hermitian
    ρ_M_0 = M_0(N, η) @ ρ @ M_0(N, η)
    if truncate:
        # numerical precision limit with float64
        # tol = 1e-15
        # since si^2 = 1e-4, this should be enough (TODO: check the size of the combinatorial factor)
        kmax = min(N - 1, int(np.log(tol) / np.log(η)))
        # print(kmax, η**kmax, tol)
    else:
        kmax = N - 1

    # This matrix multiplication is the dominant slow down, TODO: determine how it may be sped up
    # renormalise the final matrix since M was not truncated

    ρ_final = np.sum(K_k(N, η, k) @ ρ_M_0 @ dag(K_k(N, η, k)) for k in range(kmax + 1))

    if renormalise:
        return normalise(N, ρ_final)
    else:
        return ρ_final


def loss_dual_kraus(N, X, η, truncate=True):
    if truncate:
        tol = 1e-6
        kmax = int(np.log(tol) / np.log(η))
    else:
        kmax = N - 1
    X_K = np.sum(dag(K_k(N, η, k)) @ X @ K_k(N, η, k) for k in range(kmax + 1))
    return M_0(N, η) @ X_K @ M_0(N, η)


def construct_M_k_cache(N, η, kmax):
    def M_k_cached(N, η, k, cache):
        """Calculate loss Kraus operator recursively with caching."""
        if k in cache.keys():
            # print(f'k={k} already cached')
            return cache[k]
        else:
            # print(f'k={k} now calculating')
            cache[k] = (
                1
                / math.sqrt(k)
                * (math.sqrt(η / (1 - η)) * a(N))
                @ M_k_cached(N, η, k - 1, cache)
            )
            # expected to break at k=209
            if not np.all(np.isfinite(cache[k])):
                raise ValueError("K_k diverging.")
            return cache[k]

    cache = {0: M_0(N, η)}
    kmax = min(N - 1, kmax)
    _ = M_k_cached(N, η, kmax, cache)
    return cache


def loss_kraus_cached(N, state, cache, renormalise=True):
    """Use cached loss Kraus operators to calculate state after loss."""
    ρ = ρ_from_state(N, state)

    kmax = max(cache.keys())
    # print(f'Truncating losses at {kmax}.')
    # TODO: deal with DeprecationWarning from np.sum(generator)
    ρ_final = np.sum(cache[k] @ ρ @ dag(cache[k]) for k in range(kmax + 1))

    if renormalise:
        return normalise(N, ρ_final)
    else:
        return ρ_final
