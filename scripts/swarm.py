#!/usr/bin/env python3
import sys, os

# add directory to import path
sys.path.insert(1, os.path.join(sys.path[0], "../python_source"))
# sys.path.insert(1, "/central/groups/RelativityTheory/jwgardne/pleasantpheasant/python_source")

import pickle
import math
import numpy as np

from states import fock_ket, average_number, normalise, real_to_complex
from loss_channels import construct_M_k_cache
from encoding_channels import construct_total_channel
from fisher_information import QFI_finite_difference
from pso import pso

# -----------------------
# ---- user settings ----
# -----------------------
label = f"{os.environ['SLURM_JOB_ID']}"
k = 20
# N = 100
# N = 500
# N = 800
N = 900
# N = 1000
# num_peaks = 10
# num_peaks = 15
# num_peaks = 20
num_peaks = N // k + 1 if N % k != 0 else N // k
# loss_tol = 1e-50
# loss_tol = 1e-100
# loss_tol = 1e-150
kmax = 200
# with_phases = False
with_phases = True

# specific options to swarm
# number of cores you have allocated for your slurm task:
number_of_cores = int(os.environ["SLURM_CPUS_PER_TASK"])
# maxiter = 100
maxiter = 1000
swarmsize = 100
print_every = 5
# print_every = 10
# print_every = 50

options = dict(
    k=k,
    kmax=kmax,
    num_peaks=num_peaks,
    with_phases=with_phases,
    number_of_cores=number_of_cores,
    maxiter=maxiter,
    swarmsize=swarmsize,
    print_every=print_every,
)
# -----------------------
# -----------------------

if k * num_peaks > 500 - 10:
    # reduce num_peaks if necessary for N, k
    num_peaks = N // k + 1
    if N % k == 0:
        num_peaks -= 1
else:
    # reduce N to save on compute time given k, num_peaks
    N = min(500, k * num_peaks + 10)

settings = dict(
    N=N,
    σ=1e-3,
    δσ=1e-5,
    η=1e-1,
    small_signal=True,
    cached_losses=True,
)

print(
    f"State comprises {num_peaks} peaks separated by k = {k} from |{0}> to |{(num_peaks - 1) * k}> inside the truncated Hilbert space of dimension N = {N}.\nOptimiser set for {maxiter} maximum iterations with a loss tolerance of {kmax} for η = {settings['η']} and with complex phases = {with_phases}."
)
print(f"{number_of_cores} cores are available to the swarm.")

# TODO: see if the memory requirement need to increase for the cache
cache = construct_M_k_cache(N, settings["η"], kmax)
print("Calculated cached loss Kraus operators.")
settings["cache"] = cache
construct_total_channel(settings)

kets = np.array([fock_ket(N, n * k) for n in range(num_peaks)])


def construct_ket(ps):
    """Return normalised ket from probabilities of all peaks, including \ket0"""
    ps /= np.sum(ps)
    return normalise(N, np.sum(np.sqrt(ps)[:, np.newaxis] * kets, axis=0))


def construct_ket_with_phases(ket_parts_peaks):
    """Return normalised ket from real 2*m vector of ket parts between -1 and 1 for m peaks"""
    # probability amplitudes for the peaks, real_to_complex still works even with the gaps
    ket_peaks = real_to_complex(ket_parts_peaks)
    # renormalise, can't we just use np.abs here?
    norm_peaks = (ket_peaks.conj() @ ket_peaks).real
    ket_peaks /= norm_peaks**0.5
    # construct ket
    return normalise(N, np.sum(ket_peaks[:, np.newaxis] * kets, axis=0))


def callback(it, g, fg):
    if it % print_every == 0:
        print("Best after iteration {:}: {:} {:}".format(it, g, fg))

        file = f"./data/swarm/{label}.npy"
        ket = construct_ket_with_phases(g) if with_phases else construct_ket(g)
        np.save(file, ket)

        nbar = average_number(settings["N"], ket)
        filename = f"./data/swarm/{label}.pkl"
        full_results = dict(
            it=it,
            g=g,
            fg=fg,
            ket=ket,
            nbar=nbar,
            settings={k: v for k, v in settings.items() if k != "channel"},
            options=options,
        )

        with open(filename, "wb") as file:
            pickle.dump(full_results, file)


# def norm_constraint(ps):
#     return 1 - np.sum(ps)

if with_phases:

    def objective(ket_parts_peaks):
        return -QFI_finite_difference(
            construct_ket_with_phases(ket_parts_peaks), settings
        )

    lower_bounds = [-1 for _ in range(2 * num_peaks)]
    upper_bounds = [1 for _ in range(2 * num_peaks)]
else:

    def objective(ps):
        return -QFI_finite_difference(construct_ket(ps), settings)

    lower_bounds = [0 for _ in range(num_peaks)]
    upper_bounds = [1 for _ in range(num_peaks)]

# constraints = [norm_constraint]

# TODO: add checkpoint print outs/callback after every e.g. 100 iterations
ps_opt, obj_opt = pso(
    objective,
    lower_bounds,
    upper_bounds,
    # ieqcons=constraints,
    swarmsize=swarmsize,
    maxiter=maxiter,
    processes=number_of_cores,
    debug=True,
    print_every=print_every,
    callback=callback,
)

nbar = (
    average_number(settings["N"], construct_ket_with_phases(ps_opt))
    if with_phases
    else average_number(settings["N"], construct_ket(ps_opt))
)
print(f"QFI = {-obj_opt:.3f}, nbar = {nbar:.2f}, ps = {ps_opt}.")

callback(maxiter, ps_opt, obj_opt)
