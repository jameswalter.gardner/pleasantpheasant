#!/usr/bin/env python3
import sys, os

# add directory to import path
sys.path.insert(1, os.path.join(sys.path[0], "../python_source"))

import time
import math
import numpy as np
from states import fock_state, normalise
from loss_channels import loss_kraus, loss_master_eq

# from encoding_channels import construct_total_channel
from fisher_information import apply_LSO, fidelity

# -----------------------
# ---- user settings ----
# -----------------------
N = 150
settings = dict(
    N=N,
    η=1e-1,
    loss_tol=1e-150,
    # just the loss channel
    σ=0,
    small_signal=True,
)
# -----------------------
# -----------------------

ρ = fock_state(N, 8)

file = f'./data/LSOs/loss_LSO_N={settings["N"]},eta={settings["η"]},loss_tol={-int(math.log10(settings["loss_tol"]))}.npy'

ts = [time.time()]
Λη_LSO = np.load(file)
ts.append(time.time())
print(f"Loaded LSO file. Took {ts[-1] - ts[-2]:.2f}s.")

ρ_after_loss_LSO = normalise(N, apply_LSO(N, Λη_LSO, ρ))
ts.append(time.time())
print(f"Calculated loss via LSO. Took {ts[-1] - ts[-2]:.2f}s.")

ρ_after_loss_Kraus = normalise(
    N, loss_kraus(N, ρ, settings["η"], tol=settings["loss_tol"])
)
ts.append(time.time())
print(f"Calculated loss via Kraus representation. Took {ts[-1] - ts[-2]:.2f}s.")

print(
    "Fidelity between states after loss using LSO and Kraus methods: ",
    fidelity(ρ_after_loss_LSO, ρ_after_loss_Kraus),
)

print(
    "Fidelity between state after loss using Kraus method and the initial state: ",
    fidelity(ρ, ρ_after_loss_Kraus),
)

print("Fidelity between initial state and itself: ", fidelity(ρ, ρ))
print(
    "Fidelity between final Kraus state and itself: ",
    fidelity(ρ_after_loss_Kraus, ρ_after_loss_Kraus),
)

ts.append(time.time())
print("Calculating via master equation...")
ρ_after_loss_ME = normalise(N, loss_master_eq(N, ρ, settings["η"]))
ts.append(time.time())
print(f"Calculated loss via master equation. Took {ts[-1] - ts[-2]:.2f}s.")
print(
    "Fidelity between states after loss using Kraus and master equation: ",
    fidelity(ρ_after_loss_ME, ρ_after_loss_Kraus),
)
