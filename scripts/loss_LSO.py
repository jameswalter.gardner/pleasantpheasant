#!/usr/bin/env python3
import sys, os

# add directory to import path
sys.path.insert(1, os.path.join(sys.path[0], "../python_source"))

import math
import numpy as np
from states import basis_element
from encoding_channels import construct_total_channel
from fisher_information import vectorise

# -----------------------
# ---- user settings ----
# -----------------------
# N = 200
N = 150
# N = 100
# N = 50
settings = dict(
    N=N,
    η=1e-1,
    # loss_tol=1e-200,
    loss_tol=1e-150,
    # loss_tol=1e-100,
    # loss_tol=1e-50,
    # just calculate the loss channel
    σ=0,
    small_signal=True,
)
# -----------------------
# -----------------------

# wrt where executed from: TODO use absolute paths from now on?
file = f'./data/LSOs/loss_LSO_N={settings["N"]},eta={settings["η"]},loss_tol={-int(math.log10(settings["loss_tol"]))}.npy'

# check if file already exists
if os.path.isfile(file):
    raise ValueError("File already exists.")

# renormalise has to be false here
Λη = construct_total_channel(
    settings, renormalise=False, in_place=False, σ0=settings["σ"]
)

Λη_LSO = np.zeros((N**2, N**2), dtype=complex)
for k in range(N):
    for j in range(N):
        # i = j + k * N
        # k, j = divmod(i, N)
        Λη_LSO[:, j + k * N] = vectorise(Λη(basis_element(N, j, k)))

np.save(file, Λη_LSO)
