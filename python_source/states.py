# TODO: better organise this file, separate out the handling of ket parts and separate the construction of states from operations on them

from operators import *


def state_type(N, state):
    if state.shape == (N,) and state.dtype == np.dtype("complex128"):
        return "ket"
    elif state.shape == (2 * N,) and state.dtype == np.dtype("float64"):
        return "ket_parts"
    elif state.shape == (N, N) and state.dtype == np.dtype("complex128"):
        return "ρ"
    else:
        raise ValueError(
            f"Unknown state type: N = {N} but dimension is {state.shape} and type {state.dtype}."
        )


def ρ_from_ket(ket):
    """Returns density matrix for initial state from N vector of Fock basis complex coefficients.

    Args:
        ket (Nmax-by-1): complex components of initial state in Fock basis
    """
    return np.outer(ket, np.conjugate(ket))


def real_to_complex(ket_parts):
    """Returns complex of length n from real vector of length 2n

    Need to use real vectors in brute force optimisation since scipy's minimisation only works there, see https://stackoverflow.com/a/51214326.

    Args:
        ket_parts (2 Nmax-by-1): real/imaginary parts of initial state in Fock basis
    """
    ket = ket_parts[0::2] + 1j * ket_parts[1::2]
    return ket


def ρ_from_state(N, state):
    stype = state_type(N, state)
    if stype == "ket":
        return ρ_from_ket(state)
    elif stype == "ket_parts":
        return ρ_from_ket(real_to_complex(state))
    elif stype == "ρ":
        return state
    else:
        raise ValueError("Unrecognised state type.")


def basis_element(N, j, k):
    """Returns |j><k| element of density matrix."""
    ρ = np.zeros((N, N), dtype=complex)
    ρ[j, k] = 1
    return ρ


def fock_state(N, n):
    """Returns density matrix for a Fock state |n>."""
    return basis_element(N, n, n)


def random_ket(N, seed=None):
    rng = np.random.default_rng(seed)
    # random complex values with absolute value between 0 and 1 from scaling the unit interval
    # this is a uniform distribution in the parts before being normalised
    ket = real_to_complex(normalise(N, 2 * rng.random(2 * N) - 1))
    return ket


def EV(N, state, X):
    stype = state_type(N, state)
    if stype == "ket":
        ev = state.conj() @ X @ state
    elif stype == "ρ":
        ev = np.trace(state @ X)
    else:
        raise ValueError("Unrecognised state type.")
    return ev.real


def variance(N, state, X):
    Xsqr = np.linalg.matrix_power(X, 2)
    return EV(N, state, Xsqr) - EV(N, state, X) ** 2


# TODO: extract parts logic to a separate file for the brute force optimiser (otherwise not required)
def complex_to_real(ket):
    """Returns real of length 2n from complex vector of length n"""
    ket_parts = np.empty(2 * len(ket))
    ket_parts[0::2] = np.real(ket)
    ket_parts[1::2] = np.imag(ket)
    return ket_parts


def number_probs(N, state):
    # TODO: automate this paradigm
    """Returns number basis probabilities of state."""
    stype = state_type(N, state)
    if stype == "ket_parts":
        return np.abs(real_to_complex(state)) ** 2
    elif stype == "ket":
        return number_probs(N, complex_to_real(state))
    elif stype == "ρ":
        return np.diag(state).real
    else:
        raise ValueError("Unrecognised state type.")


def shorten(ket_parts):
    """Return shortened parts missing the Nmax component."""
    return ket_parts[:-2]


def extend(N, short_parts):
    # ket_parts_from_short_parts
    """Return extended parts with norm 1 from short parts missing the missing the Nmax component."""
    # https://stackoverflow.com/a/76124870
    # This could raise a math domain error if norm > 1 numerically
    print(norm(N, short_parts, short=False), short_parts)
    missing_component = math.sqrt(1 - norm(N, short_parts, short=False))
    ket_parts = np.array([*short_parts, missing_component, 0])
    return ket_parts


def norm(N, state, short=False):
    """Returns normalisation of state squared."""
    # return sum(number_probs(state))
    stype = state_type(N, state)
    if stype == "ket_parts":
        if short:
            state = extend(N, state)
        return norm(N, real_to_complex(state))
    elif stype == "ket":
        return (state.conj() @ state).real
    elif stype == "ρ":
        # squaring this to be consistent with the kets, TODO: do this the other way round?
        return np.trace(state).real ** 2
    else:
        raise ValueError("Unrecognised state type.")


def normalise(N, state):
    """Return normalised state."""
    return state / norm(N, state) ** 0.5


def purity(ρ):
    return np.trace(np.linalg.matrix_power(ρ, 2)).real


def rank(ρ, **kwargs):
    # TODO: experiment with passing tol=1e-15 to matrix_rank
    return np.linalg.matrix_rank(ρ, **kwargs)


def zero_pad(N, ket_short):
    """Return ket with zeros appended to reach the new size of the Hilbert space."""
    if len(ket_short) > N:
        raise ValueError("Ket too long.")
    ket = np.array([*ket_short, *np.zeros(N - len(ket_short))])
    return ket


def shift(N, ket, shift_by):
    # TODO: renormalise if shifted off of the end
    ket_shifted = np.zeros_like(ket)
    ket_shifted[shift_by:] = ket[: N - shift_by]
    return ket_shifted


def average_number(N, state, short=False):
    """Returns the average occupation number (proportional to energy) given parts."""
    if short:
        state = extend(N, state)
    # could just take tr(n rho) here, would be easier
    probs = number_probs(N, state)
    return sum(np.arange(len(probs)) * probs)


def dephase(ρ):
    """Return the fully dephased state."""
    return np.diag(ρ.diagonal())


def decoherence(N, ρ, σ):
    """Partial decoherence channel, i.e. random rotation channel, limits for large $\sigma$ to dephase(ρ)."""
    if σ == 0:
        return ρ
    # ρ1 = np.zeros((N, N), dtype=ρ.dtype)
    ρ1 = np.zeros_like(ρ)
    for i in range(N):
        for j in range(N):
            ρ1[i, j] += ρ[i, j] * np.exp(-0.5 * σ**2 * (i - j) ** 2)
    return ρ1


def ramanujan_poly(n):
    """Term in Ramanujan's approximation to the Factorial"""
    return (((8 * n + 4) * n + 1) * n + 1 / 30.0) ** (1.0 / 6.0)


def factorial_ramanujan(n):
    if n < 50:
        return math.factorial(n)
    else:
        # Better than Stirling's approximation, see https://www.johndcook.com/blog/2012/09/25/ramanujans-factorial-approximation/
        return math.sqrt(math.pi) * (n / math.e) ** n * ramanujan_poly(n)


def SMSV_wf_in_Fock_basis(r, θ, n):
    """Wavefunction complex amplitude for the |2n> Fock basis element.

    For n >= 50, uses Ramanujan's approximation to the factorial."""
    if n < 50:
        return (
            np.cosh(r) ** -0.5
            * (-np.exp(1j * θ) * np.tanh(r)) ** n
            * math.sqrt(math.factorial(2 * n))
            / (2**n * math.factorial(n))
        )
    else:
        return (
            np.cosh(r) ** -0.5
            * (-np.exp(1j * θ) * np.tanh(r)) ** n
            * ramanujan_poly(2 * n) ** 0.5
            / (np.pi**0.25 * ramanujan_poly(n))
        )


def pure_state_ket_parts(
    N, state_label, state_settings=None, print_norm=False, return_ket=False
):
    """Return Fock basis coefficients for common pure states."""
    ket_parts = None
    if state_label == "Fock":
        ket = np.zeros(N, dtype=complex)
        ket[state_settings["n"]] = 1 + 0j

    elif state_label == "vacuum":
        return pure_state_ket_parts(N, "Fock", dict(n=0), print_norm, return_ket)

    elif state_label == "coherent":
        α = state_settings["α"]
        normalisation = np.exp(-np.abs(α) ** 2 / 2)
        # np.sqrt is much slower than math.sqrt on large numbers
        # TODO: resolve inaccuracy of sqrt on large integers, see https://stackoverflow.com/questions/47854635/square-root-of-a-number-greater-than-102000-in-python-3, or perhaps take product of sqrts instead?
        ket = np.array([α**n / math.sqrt(math.factorial(n)) for n in range(N)])
        ket *= normalisation

    elif state_label == "cat":
        α = state_settings["α"]
        pm = state_settings["pm"]
        normalisation = (2 * (1 + pm * np.exp(-2 * np.abs(α) ** 2))) ** -0.5
        ket_parts_p = pure_state_ket_parts(
            N, "coherent", state_settings=dict(α=α), print_norm=False
        )
        ket_parts_m = pure_state_ket_parts(
            N, "coherent", state_settings=dict(α=-α), print_norm=False
        )
        ket_parts = ket_parts_p + pm * ket_parts_m
        ket_parts *= normalisation

    elif state_label == "SMSV":
        r = state_settings["r"]
        θ = state_settings["θ"]
        ket = np.zeros(N, dtype=complex)
        assert N % 2 == 0
        ket[::2] = np.array([SMSV_wf_in_Fock_basis(r, θ, n) for n in range(int(N / 2))])

    if ket_parts is None:
        ket_parts = complex_to_real(ket)

    # re-normalise due to truncation
    renormalisation = norm(N, ket_parts)
    if print_norm:
        print(
            f"Dimension N={N} {state_label} state with {state_settings} leaves norm of: {renormalisation:.5f}"
        )
    # probability amplitudes rescaled by sqrt of norm
    ket_parts *= renormalisation**-0.5

    if return_ket:
        return real_to_complex(ket_parts)
    else:
        return ket_parts


def pure_state_ket(*args, **kwargs):
    return pure_state_ket_parts(*args, **kwargs, return_ket=True)


def fock_ket(N, n):
    return pure_state_ket(N, "Fock", dict(n=n))


def vacuum(N):
    return fock_ket(N, 0)


def thermal_state(N, nbar=None, r=None):
    ρ = np.zeros((N, N), dtype=complex)
    if nbar is None and r is not None:
        nbar = np.sinh(r) ** 2
    elif nbar is None and r is None:
        raise ValueError("nbar not provided.")
    for n in range(N):
        ρ[n, n] = nbar**n / (1 + nbar) ** (1 + n)
    return ρ


def displace(N, state, α, truncation=None):
    """Returns the state displaced by D(α)."""
    U = displacement_operator(N, α, truncation)

    stype = state_type(N, state)
    if stype == "ket":
        return U @ state
    elif stype == "ρ":
        return U @ state @ dag(U)


def X_gate(N, state, q, truncation=None):
    """Tzitrin+20 Eq. 4"""
    return displace(N, state, α=q / np.sqrt(2), truncation=truncation)


def Z_gate(N, state, p, truncation=None):
    """Tzitrin+20 Eq. 4"""
    return displace(N, state, α=1j * p / np.sqrt(2), truncation=truncation)


def squeeze(N, state, r, θ, truncation=None):
    """Returns the state squeezed by S(r,θ)."""
    z = r * np.exp(1j * θ)
    A = 0.5 * (
        z.conjugate() * np.linalg.matrix_power(a(N), 2)
        - z * np.linalg.matrix_power(ad(N), 2)
    )
    U = exp_matrix(A, truncation=truncation)

    stype = state_type(N, state)
    if stype == "ket":
        return U @ state
    elif stype == "ρ":
        return U @ state @ dag(U)


def number_preserving(N, state, r, θ, truncation=None):
    """OAT is the GOAT"""
    # TODO: determine what this physically is, or replace it with whatever χ^3 is meant to be, look at the wigner plane
    # θ is not used here
    H = r * np.linalg.matrix_power(ad(N), 2) @ np.linalg.matrix_power(a(N), 2)
    U = exp_matrix(-1j * H, truncation=truncation)

    stype = state_type(N, state)
    if stype == "ket":
        return U @ state
    elif stype == "ρ":
        return U @ state @ dag(U)


def rotate(N, state, θ, truncation=None):
    """Returns the state rotated by R(θ)."""
    H = θ * Nhat(N)
    U = exp_matrix(-1j * H, truncation=truncation)

    stype = state_type(N, state)
    if stype == "ket":
        return U @ state
    elif stype == "ρ":
        return U @ state @ dag(U)
