from operators import *
from states import *
from loss_channels import *
from encoding_channels import *
from fisher_information import *

# TODO: find a cleaner solution for this to avoid importing qutip on HPC
# from plotters import *
# from tqdm.notebook import tqdm
# import pandas as pd


# TODO: functionalise the LSO form of G as well, precalculate the LSOs (accept them as input here?)
def G(X, settings, Λ_dual=None, Λ_dot_dual=None, small_signal=False):
    Xsqr = np.linalg.matrix_power(X, 2)
    if Λ_dual is None or Λ_dot_dual is None:
        Λη_dual = lambda X: loss_dual_kraus(settings["N"], X, settings["η"])

        if not small_signal:
            Λσ = lambda ρ: encoding_channel(
                settings["N"],
                ρ,
                settings["σ"],
                encoding_truncation=settings["encoding_truncation"],
                renormalise=False,
            )
            Λσ_dot = lambda ρ: encoding_channel(
                settings["N"],
                ρ,
                settings["σ"],
                encoding_truncation=settings["encoding_truncation"],
                renormalise=False,
                use_pdot=True,
            )
        else:
            Λσ = lambda ρ: small_signal_encoding_channel(
                settings["N"], ρ, settings["σ"]
            )
            Λσ_dot = lambda ρ: small_signal_encoding_channel(
                settings["N"], ρ, settings["σ"], dot=True
            )

        return Λη_dual(-1 * Λσ(Xsqr) + 2 * Λσ_dot(X))
    else:
        return -1 * Λ_dual(Xsqr) + 2 * Λ_dot_dual(X)


def next_state_from_SLD(SLD, settings, return_ket=False, **LSO_kwargs):
    G_SLD = G(SLD, settings, **LSO_kwargs)
    # TODO: check that the preimage of the SLD is Hermitian such that I can use .eig instead of .eigh
    evals, evecs = np.linalg.eigh(G_SLD)
    # evec with largest magnitude eval is the next pure state, remember to index correctly
    # The normalized (unit "length") eigenvectors, such that the column v[:,i] is the eigenvector corresponding to the eigenvalue w[i].
    # ket = evecs[:, np.argmax(np.abs(evals))]
    ket = evecs[:, np.argmax(evals)]
    if return_ket:
        return ket
    else:
        ρ_next = ρ_from_ket(ket)
        return ρ_next


def next_state_from_given_state(
    ket, settings, Λ_dual=None, Λ_dot_dual=None, small_signal=False, **kwargs
):
    # working with ket since the states are pure
    ρ = ρ_from_ket(ket)
    SLD = sld(ρ, settings, **kwargs)
    ket_next = next_state_from_SLD(
        SLD,
        settings,
        return_ket=True,
        Λ_dual=Λ_dual,
        Λ_dot_dual=Λ_dot_dual,
        small_signal=small_signal,
    )
    return ket_next


def variational_algorithm(
    ket0,
    settings,
    num_iter=2,
    plot=True,
    if_plot_then_plot_imshow=False,
    running_prints=False,
    print_every=None,
    run_from_script=False,
    Λ_LSO=None,
    Λ_dot_LSO=None,
    Λ_dual=None,
    Λ_dot_dual=None,
    return_data_as_dict=False,
    calculate_QFI=True,
    small_signal=False,
    file_to_save_final_state=None,
    return_final_ket=False,
    **sld_kwargs,
):
    # TODO: write docstring
    # find optimal Fock state to compare to
    if calculate_QFI:
        n_Fock_opt, QFI_Fock_opt = 0, 0
        n_exp = int(-1 - 1 / math.log(1 - settings["η"]))
        for n in range(n_exp - 2, n_exp + 3):
            state = fock_state(n)
            if Λ_LSO is not None and Λ_dot_LSO is not None:
                QFI = QFI_from_LSO(state, Λ_LSO, Λ_dot_LSO)
            else:
                if n == n_exp - 2:
                    print("Using slower (but more accurate?) finite difference method.")
                QFI = QFI_finite_difference(state, settings)
            if QFI > QFI_Fock_opt:
                QFI_Fock_opt = QFI
                n_Fock_opt = n
            else:
                # monotonicity
                break
        print(
            f"Optimal Fock state for settings is |{n_Fock_opt}> with QFI = {QFI_Fock_opt:.2f}."
        )

    if not run_from_script:
        kets = []
        if calculate_QFI:
            qfis = []
            qfi_ratios = []
        itr = tqdm(range(num_iter + 1), desc="Variational algorithm")
    else:
        itr = range(num_iter + 1)

    for i in itr:
        if i == 0:
            ket = ket0
        else:
            ket = next_state_from_given_state(
                kets[-1] if not run_from_script else ket,
                settings,
                Λ_dual=Λ_dual,
                Λ_dot_dual=Λ_dot_dual,
                small_signal=small_signal,
                **sld_kwargs,
            )

        if not run_from_script:
            kets.append(ket)

        if calculate_QFI:
            # results are not recovered by later QFI calls?
            if Λ_LSO is not None and Λ_dot_LSO is not None:
                qfi_i = QFI_from_LSO(ket, Λ_LSO, Λ_dot_LSO)
            else:
                # qfi_i = FI_total_channel_from_state(ket, settings)
                qfi_i = QFI_finite_difference(ket, settings)
            qfis.append(qfi_i)
            qfi_ratios.append(qfi_i / QFI_Fock_opt)

        if running_prints or plot:
            message = (
                f"Iteration {i}: QFI = {qfis[i]:.3f}"
                if calculate_QFI
                else f"Iteration {i}"
            )

        if running_prints:
            print(message)

        # print(i)
        # print(i, QFI_finite_difference(ket, settings))
        if print_every is not None and (i % print_every == 0 or i == num_iter):
            Fq = QFI_finite_difference(ket, settings)
            message = f"Iteration {i}: QFI = {Fq:.3f}, ket = {ket}"
            print(message, flush=True)

        if plot:
            # TODO: also plot the final state
            fig, _ = plot_wigner(ket, title=message)
            plt.show(fig)

            if if_plot_then_plot_imshow:
                title = f"Iteration {i}: " + r"SLD, $L$"
                fig, _ = plot_imshow(
                    sld(ρ_from_ket(ket), settings, **sld_kwargs), title
                )
                plt.show(fig)

    if not run_from_script:
        if return_data_as_dict:
            data = {}
            for i in range(num_iter + 1):
                data[i] = dict(qfi=qfis[i], ket=kets[i])
            return data
        else:
            df = pd.DataFrame()
            df["itr"] = range(num_iter + 1)
            if calculate_QFI:
                df["QFI"] = qfis
                df["QFI/Fock"] = qfi_ratios
            df["ket"] = kets
            if calculate_QFI:
                df = df.sort_values("QFI", ascending=False)
            return df

    if file_to_save_final_state is not None:
        np.save(file_to_save_final_state, ket)

    if return_final_ket:
        return ket
