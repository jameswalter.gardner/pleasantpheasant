#!/usr/bin/env python3
import sys, os

# add directory to import path
sys.path.insert(1, os.path.join(sys.path[0], "../python_source"))

import pickle
from pathlib import Path
import math
import numpy as np
from states import fock_ket, state_type
from variational_algorithm import variational_algorithm

# -----------------------
# ---- user settings ----
# -----------------------
num_iter = 10000
print_every = 500
settings = dict(
    N=None,
    σ=1e-3,
    δσ=1e-5,
    η=1e-1,
    small_signal=True,
    loss_tol=1e-100,
)
# -----------------------
# -----------------------

# start_state_label = "vacuum"
# ket0 = fock_ket(settings["N"], 0)

# TODO: run from the optimal state found using gradient descent
input_file = f"./data/gradient_descent/ket-N=385,k=25,peaks=15,it=313,QFI=17.8.npy"
start_state_label = f"[{Path(input_file).stem}]"
ket0 = np.load(input_file)
# TODO: state type error raised and process failed, see stderr_JOB_39457581.txt, due to the N not matching.
# TODO: update N to match the loaded ket's dimension
N = len(ket0)
assert state_type(N, ket0) == "ket"
if settings["N"] is None or settings["N"] == N:
    settings["N"] = N
else:
    raise ValueError("N must match ket loaded.")

# TODO: compress file name and keep this information available, e.g., in a .h5 file
label = f"ket-N={settings['N']},start={start_state_label},itr={num_iter},loss_tol={-int(math.log10(settings['loss_tol']))}"
output_file = f"./data/variational_algorithm/{label}.npy"

variational_algorithm_kwargs = dict(
    num_iter=num_iter,
    print_every=print_every,
    run_from_script=True,
    small_signal=True,
    plot=False,
    running_prints=False,
    calculate_QFI=False,
    file_to_save_final_state=output_file,
    return_final_ket=True,
)

ket = variational_algorithm(ket0, settings, **variational_algorithm_kwargs)

filename = f"./data/variational_algorithm/{os.environ['SLURM_JOB_ID']}.pkl"
full_results = dict(
    initial_ket=ket0,
    final_ket=ket,
    settings=settings,
    variational_algorithm_kwargs=variational_algorithm_kwargs,
)
with open(filename, "wb") as file:
    pickle.dump(full_results, file)

# with open(filename, 'rb') as file:
#     full_results = pickle.load(file)
