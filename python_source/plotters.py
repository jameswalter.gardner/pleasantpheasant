import math
import numpy as np

# from qutip import Qobj, plot_wigner_fock_distribution
from qutip import Qobj, isket, ket2dm, plot_fock_distribution
from qutip import plot_wigner as plot_wigner_qutip
import matplotlib.pyplot as plt

# LaTeX fonts
import matplotlib

matplotlib.rcParams["mathtext.fontset"] = "cm"
matplotlib.rcParams["font.family"] = "STIXGeneral"
plt.rcParams.update({"font.size": 18})


def print_error(x1, x2, dp=2, tol=1e-15):
    error = x2 - x1
    print(
        f"Equal: {np.abs(error) < tol}. Error = {error:.{dp}g}. Value 1 = {x1:.{dp}g}. Value 2 = {x2:.{dp}g}."
    )


def plot_wigner_fock_distribution(
    rho,
    fig=None,
    axes=None,
    figsize=(8, 4),
    cmap=None,
    alpha_max=7.5,
    colorbar=False,
    method="iterative",
    projection="2d",
):
    """
    From <https://qutip.org/docs/4.2/modules/qutip/visualization.html#plot_wigner_fock_distribution>
    Since removed by 5.0.x <https://qutip.readthedocs.io/en/qutip-5.0.x/_modules/qutip/visualization.html>

    Plot the Fock distribution and the Wigner function for a density matrix
    (or ket) that describes an oscillator mode.

    Parameters
    ----------
    rho : :class:`qutip.qobj.Qobj`
        The density matrix (or ket) of the state to visualize.

    fig : a matplotlib Figure instance
        The Figure canvas in which the plot will be drawn.

    axes : a list of two matplotlib axes instances
        The axes context in which the plot will be drawn.

    figsize : (width, height)
        The size of the matplotlib figure (in inches) if it is to be created
        (that is, if no 'fig' and 'ax' arguments are passed).

    cmap : a matplotlib cmap instance
        The colormap.

    alpha_max : float
        The span of the x and y coordinates (both [-alpha_max, alpha_max]).

    colorbar : bool
        Whether (True) or not (False) a colorbar should be attached to the
        Wigner function graph.

    method : string {'iterative', 'laguerre', 'fft'}
        The method used for calculating the wigner function. See the
        documentation for qutip.wigner for details.

    projection: string {'2d', '3d'}
        Specify whether the Wigner function is to be plotted as a
        contour graph ('2d') or surface plot ('3d').

    Returns
    -------
    fig, ax : tuple
        A tuple of the matplotlib figure and axes instances used to produce
        the figure.
    """

    if not fig and not axes:
        if projection == "2d":
            fig, axes = plt.subplots(1, 2, figsize=figsize)
        elif projection == "3d":
            fig = plt.figure(figsize=figsize)
            axes = [fig.add_subplot(1, 2, 1), fig.add_subplot(1, 2, 2, projection="3d")]
        else:
            raise ValueError("Unexpected value of projection keyword argument")

    if isket(rho):
        rho = ket2dm(rho)

    plot_fock_distribution(rho, fig=fig, ax=axes[0])
    # plot_wigner(rho, fig=fig, ax=axes[1], figsize=figsize, cmap=cmap,
    #             alpha_max=alpha_max, colorbar=colorbar, method=method,
    #             projection=projection)
    xvec = np.linspace(-alpha_max, alpha_max, 200)
    yvec = np.linspace(-alpha_max, alpha_max, 200)
    plot_wigner_qutip(
        rho,
        fig=fig,
        ax=axes[1],
        cmap=cmap,
        colorbar=colorbar,
        method=method,
        projection=projection,
        xvec=xvec,
        yvec=yvec,
    )

    return fig, axes


def plot_wigner(
    N,
    state,
    title=None,
    title_y=1.05,
    alpha_max=None,
    method="iterative",
    figInput=None,
    axesInput=None,
):
    # TODO: scale the scale of the wigner axes with Nmax and the Fock axes with the maximum probability (could also show the probabilities logarithmically)
    # TODO: show colorbar for the scale of the WF (whether Linf normalised)
    fig, ax = plot_wigner_fock_distribution(
        Qobj(state).unit(),
        fig=figInput,
        axes=axesInput,
        # alpha_max=7.5
        alpha_max=max(7.5, 1.5 * math.sqrt(N - 1)) if alpha_max is None else alpha_max,
        # TODO: fix aspect ratio with colorbar by passing fig, ax with grid_spec pre-set
        # colorbar=True
        method=method,
    )
    if title is not None:
        fig.suptitle(title, y=title_y)
    return fig, ax


def plot_imshow(X, title="", no_show=False):
    fig, axs = plt.subplots(
        1,
        3,
        figsize=(12, 3),
        gridspec_kw={
            "wspace": 0.4,
            # "hspace": 0.05,
        },
    )
    axs = list(reversed(axs))

    im = axs[0].imshow(X.real)
    fig.colorbar(im, ax=axs[0], label="real component")
    axs[0].set(xlabel="n", ylabel="m")

    # TODO: plot eigenvalues instead
    data = X.diagonal().real
    xaxis = np.arange(len(data))
    # TODO: handle zero values
    datas = [
        [xaxis[data != 0], np.abs(data[data != 0])],
        [xaxis[data > 0], np.abs(data[data > 0])],
        [xaxis[data < 0], np.abs(data[data < 0])],
    ]
    axs[1].semilogy(datas[0][0], datas[0][1], "-", color="grey")
    axs[1].semilogy(datas[1][0], datas[1][1], ".", color="b", label=r"$+$")
    axs[1].semilogy(datas[2][0], datas[2][1], ".", color="r", label=r"$-$")
    axs[1].legend(
        title="sign",
        handlelength=1,
        labelspacing=0,
        frameon=False,
        handletextpad=0.3,
        fontsize=18,
    )
    axs[1].set(xlabel="n", ylabel="magnitude")
    axs[1].grid("both", "both", color="gainsboro")

    axs[2].plot(xaxis, data, ".-", color="k")
    axs[2].set(xlabel="n", ylabel="signed value")
    axs[2].grid("both", "both", color="gainsboro")

    fig.suptitle(title, y=1.05)

    if not no_show:
        plt.show(fig)
    return fig, axs
