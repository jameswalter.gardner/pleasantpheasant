#!/usr/bin/env python3
import sys, os

# add directory to import path
sys.path.insert(1, os.path.join(sys.path[0], "../python_source"))
# sys.path.insert(1, "/central/groups/RelativityTheory/jwgardne/pleasantpheasant/python_source")

import pickle
import math
import numpy as np

from operators import *
from states import *
from loss_channels import construct_M_k_cache
from encoding_channels import construct_total_channel
from fisher_information import QFI_finite_difference
from pso import pso

# -----------------------
# ---- user settings ----
# -----------------------
label = f"{os.environ['SLURM_JOB_ID']}"
N = 500
# loss_tol = 1e-100
kmax = 200
num_stack = 20

# specific options to swarm
number_of_cores = int(os.environ["SLURM_CPUS_PER_TASK"])
print(f"{number_of_cores} cores are available to the swarm.")
maxiter = 1000
swarmsize = 100
# print_every = 10
print_every = 5

options = dict(
    jobid=os.environ["SLURM_JOB_ID"],
    number_of_cores=number_of_cores,
    maxiter=maxiter,
    swarmsize=swarmsize,
    print_every=print_every,
)
# -----------------------
# -----------------------

settings = dict(
    N=N,
    σ=1e-3,
    δσ=1e-5,
    η=1e-1,
    small_signal=True,
    cached_losses=True,
)
cache = construct_M_k_cache(N, settings["η"], kmax)
print("Calculated cached loss Kraus operators.")
settings["cache"] = cache
construct_total_channel(settings)


def triple(N, state, r, θ, truncation=None):
    # TODO: determine what this physically is, or replace it with whatever χ^3 is meant to be, look at the wigner plane
    z = r * np.exp(1j * θ)
    H = (
        1.0
        / 3.0
        * (
            z.conjugate() * np.linalg.matrix_power(a(N), 3)
            - z * np.linalg.matrix_power(ad(N), 3)
        )
    )
    U = exp_matrix(H, truncation=truncation)

    stype = state_type(N, state)
    if stype == "ket":
        return U @ state
    elif stype == "ρ":
        return U @ state @ dag(U)


# start from the vacuum and apply a stack of accessible unitaries
ket0 = fock_ket(settings["N"], 0)

# two real DOF for each unitary in the stack
num_gs = 2 * num_stack
# can't truncate early if rs are large
truncation = None

stack = list(range(num_stack))
fn1 = lambda ket, g1, g2: squeeze(N, ket, g1, g2, truncation=truncation)
stack[::2] = [fn1 for _ in stack[::2]]
# need non-Gaussian terms in the stack of unitaries, e.g. χ^(3)
# fn2 = lambda ket, g1, g2: triple(N, ket, g1, g2, truncation=truncation)
# print('Alternating squeezing and χ^(3).')
fn2 = lambda ket, g1, g2: number_preserving(N, ket, g1, g2, truncation=truncation)
print("Alternating squeezing and number preserving.")
stack[1::2] = [fn2 for _ in stack[1::2]]


def construct_ket(gs):
    """Return normalised ket from list of coupling constants for the stack applied to the vacuum."""
    ket = ket0
    for i in range(num_stack):
        g1 = gs[2 * i]
        g2 = gs[2 * i + 1]
        ket = stack[i](ket, g1, g2)
    return normalise(N, ket)


def objective(gs):
    return -QFI_finite_difference(construct_ket(gs), settings)


def callback(it, g, fg):
    if it % print_every == 0:
        print("Best after iteration {:}: {:} {:}".format(it, g, fg))

        file = f"./data/accessible/{label}.npy"
        ket = construct_ket(g)
        np.save(file, ket)

        nbar = average_number(settings["N"], ket)
        filename = f"./data/accessible/{label}.pkl"
        full_results = dict(
            it=it,
            g=g,
            fg=fg,
            ket=ket,
            nbar=nbar,
            settings={k: v for k, v in settings.items() if k != "channel"},
            options=options,
        )

        with open(filename, "wb") as file:
            pickle.dump(full_results, file)


# TODO: normalise the range for the various coupling constants
# angles go from 0 to 2 pi, constants go from 0 to 10
lower_bounds = np.zeros(num_gs)
lower_bounds[1::2] = -math.pi
upper_bounds = np.full(num_gs, 10)
upper_bounds[1::2] = math.pi
# print("Bounds: ", lower_bounds, upper_bounds)

gs_opt, obj_opt = pso(
    objective,
    lower_bounds,
    upper_bounds,
    swarmsize=swarmsize,
    maxiter=maxiter,
    processes=number_of_cores,
    debug=True,
    print_every=print_every,
    callback=callback,
)

nbar = average_number(settings["N"], construct_ket(gs_opt))
print(f"QFI = {-obj_opt:.3f}, nbar = {nbar:.2f}, gs = {gs_opt}.")

callback(maxiter, gs_opt, obj_opt)
