#!/bin/bash
#
#SBATCH --job-name=access
#SBATCH --output=slurm_output/accessible/stdout_JOB_%A.txt
#SBATCH --error=slurm_output/accessible/stderr_JOB_%A.txt
#
#SBATCH --time=07-00:00:00 #00-08:00:00 # DD-HH:MM:SS
#SBATCH --mem-per-cpu=250 # MB, TODO: use mprof to check required memory and time
#
# # https://www.hpc.caltech.edu/documentation/slurm-commands
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=32

srun python3 -u ./scripts/accessible_swarm.py
