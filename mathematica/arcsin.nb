(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      6107,        153]
NotebookOptionsPosition[      5442,        132]
NotebookOutlinePosition[      5806,        148]
CellTagsIndexPosition[      5763,        145]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"ArcSin", "[", 
     RowBox[{"1", "-", "\[Epsilon]"}], "]"}], "/", 
    RowBox[{"(", 
     RowBox[{"\[Pi]", "/", "2"}], ")"}]}], ",", 
   RowBox[{"{", 
    RowBox[{"\[Epsilon]", ",", "0", ",", "0.015"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.8944623668333883`*^9, 3.8944624020285263`*^9}, {
  3.894462437243595*^9, 3.894462483391739*^9}},
 CellLabel->"In[8]:=",ExpressionUUID->"ae5c2d92-8713-48c0-b0d8-771064adfd55"],

Cell[BoxData[
 GraphicsBox[{{{}, {}, 
    TagBox[
     {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
      1.], LineBox[CompressedData["
1:eJwV1Hs0lVkfB3BTmpjECTUuoeMyDCLjMijne8ggB72IMS0qMwmFlHJPh9wv
T3WEk2qQdZqIUZRbxt30DtNQNDM5iCjkec6D0oyU3uf9Y6+9Pn/89u+792+t
zf7+uFfQGikpqXBm/X83qItd9JddspsvU/xn7CMNXqhbnazbICdCbkbbZJ7G
/lRB6TPeJEfBStBYOktDfigpuHmB5FhbX790eYrGxEXLol7ea46q0DDGf5xG
S9Otdw2a/3IKa+ZOlY7QKJxgHxAtvOf4i5QMuE9piFQyFNt5a1EXvELYD9IQ
GDt0W91bB/0xg/vZAzT43A+nqzVloFbfmbP8kOkfEvW0eEEOWzzabM1/o7F+
xHSgmaeEnAdaS43tNFaDNn4ZzVfGUEt58K+tNN7Qr5LN7m1GT1/P3uctNJ6v
vWF+U1MFTYaeL7lNTD6jrUUFC1tRusHWgV9Lo9FO2rSRpwNWAO9dyg0adWui
xJtf6kDXe0OPlojGzw8mMqL4ulie8J3uKGfye7aNm9zTw9iosfPmMiZ/ULxA
pGmAheKYkcUrNMLy5pcEC8bY/TmPfURA40JExFg9zxwTh3MPnEimMf6pR/0P
Jea4rZagdohPY0fJdoL12hwGkwI1z7M0BvpJu6PFFpj9U6rG5gwNltmxaxqz
lsh7f/gXnTgaF18H+6emW+Pj2935nieY/rGBw54ddig/lLDy5/fMfVn2tavK
HBSI84veBdL4qmJb9q0QDkYTeUFajAefPrNZxwLSa1kpYQdpZGvUuPmbcvFH
tzOU/Wn0Jm2vUkiyR7yslWqhDw1Xe8Nj0eqOyGtYd8V1D42vkw2UMxwc4Rre
e/yyCw3dji9+KQp1xPDsqmTGmZkHV0ehqcERm4s1X2Q50ajlbq1b8f4GWibv
Egd301Dlyq/wc51gzheLz3FozHAWs3M+uKBgRPZ3Dwsa6buaK0ufuUNYhkpX
Ng3fnOHZg03eEITWxStKMe/pIxb2v/LDtkylRd9xCYKFlbPpXQEoMdIzvNwj
gfnVJEOFlED85aVTuFwpAVmeoqay5TAK5EyrGgQSLE2Hi7vPHMHOaTeH02ck
uDBcXBGoGooEB9+pG8ESJMjWuFzPPAaJTVU6b58EWvMa2qVz4UihV4fYDhLc
cgiWdraPxLDf404bUwmOG5mPn48+ga6qQ15cTQkS50o6B3pOIpcdy+HLMfnW
2zrMy5+Cst5cdeUKhbgO58oJ+9O4+GKrykmSwhOvtyp9RDSSXxU/Ch2h4Jei
EpneE4OoZwd2jDykoJclfmizJQ4vt51s5bdS6J/r+2/foXgoaJaU99dQKCVN
q0wLEjC5pNsXdp3C/W6LwpzJRKSJOT/uukThg211SJN6EhofTOU5ZVDQ2Oj9
d4/7Waxd94XM43gKu8Vu+i/c+CB7a/oF4RQ2fKzwHZjmQ+VLJc7JQAqxxLdZ
DeeTMVOm7J2zj8JQ35XPCIsUKKgtUeouTP3fqnz3P1Igno4ZvruLwrf/mflX
5vQ5bBoPtM7ZQeFs0XxQNSsVxyi24UE9CoW559n2FamQtpVZvaRGwYtKGh/w
SEPQAf+dcgoU2Bo38/fMpuG3Zsr8hDSFgka2a+uFdDy2rClTXyahofPkjfr2
DITNJLbfkpDI36klCfk1A7nXCj8VTJH4ua3p1Z2wTPDf/lXW8ZTEqCIhnlyf
hYCuodrefhJ3HtX2b7qdBeMRYde2HhJRnAhFU59sBETKsHubSeyJ/2GPJZ2N
5+dqdBzukOhmV5z7ujAHlQ/chz/7iYSX0KBW+6tcWJWMDly9RqK/hWP6oTcX
NkIfTswlEgtuoprByDz47egbFWWT0K/6Rjd/A4GrPi8sC5JJWBdZaznJE7Ds
ujwfxtglxUhtmUVAVK5a7cg4xG8T6+AWAlI98UZv+CQqpUdXDNkEsrvv23kz
3h5warDTioDS/TWNm86SMJcvT1kMJCAT988nFxJJOC4XnBEdJnBFu3wpmPG+
qcxYv2ACH4WKc2B8qjkiojWMgOzKTjGdQKL+iO3+7BgC9at3BvYytml7ZKad
R8Ap9a0CK56Ea2W38ZPzBAofd2rNxJHYX9CgnykgsGiy16ydcTzzPUmKCMzp
/PRdJOOWz0Nlm68TuMZ3bxuIJfH7Gn/pcBGB8nal0ZuMRymPj1o3mfMthO/5
jFe7LJbSqgmo9w5zzRjL1+jPW98m4BUtDJJlrFmsNjdXS0D/qFLuRAwJk7SN
L3+8R+DuUd7dJsaIlJrwbCRgZscdu8j4f1sgXp0=
       "]]},
     Annotation[#, "Charting`Private`Tag$4920#1"]& ]}, {}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0, 0.889595922550063},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, 
     Charting`ScaledFrameTicks[{Identity, Identity}]}, {Automatic, 
     Charting`ScaledFrameTicks[{Identity, Identity}]}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->All,
  Method->{
   "DefaultBoundaryStyle" -> Automatic, "DefaultMeshStyle" -> 
    AbsolutePointSize[6], "ScalingFunctions" -> None, 
    "CoordinatesToolOptions" -> {"DisplayFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& ), "CopiedValueFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& )}},
  PlotRange->{{0, 0.015}, {0.889595922550063, 0.9999842477455427}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.894462391894553*^9, 3.894462402462516*^9}, {
   3.894462439653742*^9, 3.894462443058861*^9}, 3.894462490358589*^9},
 CellLabel->"Out[8]=",ExpressionUUID->"7f4e4b90-79bd-4f73-907b-1730668457ee"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"1", "-", "0.986301"}]], "Input",
 CellChangeTimes->{{3.894462419681332*^9, 3.894462420187009*^9}},
 CellLabel->"In[3]:=",ExpressionUUID->"587a5b89-d6e3-448e-bba0-bc88905eae06"],

Cell[BoxData["0.013699000000000017`"], "Output",
 CellChangeTimes->{3.894462420964683*^9},
 CellLabel->"Out[3]=",ExpressionUUID->"76d548f6-ff85-4326-8ce2-bab771bd68c1"]
}, Open  ]]
},
WindowSize->{808, 911},
WindowMargins->{{489, Automatic}, {55, Automatic}},
Magnification:>2. Inherited,
FrontEndVersion->"11.3 for Linux x86 (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 498, 12, 104, "Input",ExpressionUUID->"ae5c2d92-8713-48c0-b0d8-771064adfd55"],
Cell[1081, 36, 3937, 81, 404, "Output",ExpressionUUID->"7f4e4b90-79bd-4f73-907b-1730668457ee"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5055, 122, 200, 3, 60, "Input",ExpressionUUID->"587a5b89-d6e3-448e-bba0-bc88905eae06"],
Cell[5258, 127, 168, 2, 68, "Output",ExpressionUUID->"76d548f6-ff85-4326-8ce2-bab771bd68c1"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

